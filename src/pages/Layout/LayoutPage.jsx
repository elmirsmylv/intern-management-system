import { Layout, Menu } from "antd";
import React, { useEffect, useState } from "react";
import { ProjectOutlined, UserOutlined } from "@ant-design/icons";
import {
  AiOutlineHome,
  AiOutlineMail,
  AiOutlineExport,
  AiOutlineCalendar,
} from "react-icons/ai";
import { FaUniversity } from "react-icons/fa";
import { BiLogOut } from "react-icons/bi";

import { ReactComponent as Logo } from "../../assets/images/Logo.svg";
import { ReactComponent as LogoDiamond } from "../../assets/images/LogoDiamond.svg";
import Navbar from "../../components/Navbar/Navbar";
import { Link, useLocation } from "react-router-dom";

const { Sider, Content } = Layout;

function LayoutPage({ children }) {
  const [collapsed, setCollapsed] = useState(false);

  const { pathname } = useLocation();

  const toggle = () => {
    setCollapsed(!collapsed);
  };

  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider
        theme="light"
        collapsed={collapsed}
        style={{
          height: "100vh",
          position: "fixed",
          overflow: "hidden",
          left: 0,
        }}
      >
        <Link to="/">
          <div className="logo">
            {collapsed ? (
              <LogoDiamond
                style={{ marginLeft: "1.4rem", marginBlock: "1rem" }}
              />
            ) : (
              <Logo
                style={{
                  marginLeft: "1.5rem",
                  marginBlock: "1rem",
                }}
              />
            )}
          </div>
        </Link>
        <Menu theme="light" selectedKeys={[pathname]} mode="inline">
          <Menu.Item key="/dashboard" icon={<AiOutlineHome />}>
            <Link to="/dashboard">Ana Sayfa</Link>
          </Menu.Item>
          <Menu.Item key="/interns" icon={<UserOutlined />}>
            <Link to="/interns">Stajyerler</Link>
          </Menu.Item>
          <Menu.Item key="/universities" icon={<FaUniversity />}>
            <Link to="/universities">Üniversiteler</Link>
          </Menu.Item>
          <Menu.Item key="/projects" icon={<ProjectOutlined />}>
            <Link to="/projects">Projeler</Link>
          </Menu.Item>
          <Menu.Item
            key="/logout"
            style={{ position: "absolute", bottom: "10px" }}
            icon={
              <BiLogOut
                size={19}
                style={{
                  marginLeft: "-4.5px",
                }}
              />
            }
          >
            <Link to="/logout">Logout</Link>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout
        className="site-layout"
        style={
          !collapsed
            ? { marginLeft: 200, transition: "all 0.25s" }
            : { marginLeft: 80, transition: "all 0.25s" }
        }
      >
        {/* NAVBAR */}
        <Navbar collapsed={collapsed} toggle={toggle} />

        <Content
          className="site-layout-background"
          style={{
            margin: "24px 16px",
            padding: 8,
            minHeight: 280,
          }}
        >
          {children}
        </Content>
      </Layout>
    </Layout>
  );
}

export default LayoutPage;
