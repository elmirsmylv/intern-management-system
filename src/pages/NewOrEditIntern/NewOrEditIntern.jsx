import React, { useRef, useState } from "react";
import {
  Card,
  DatePicker,
  Form,
  Input,
  Modal,
  Select,
  Slider,
  Spin,
  Upload,
} from "antd";
import { useHistory, useLocation, useParams, Redirect } from "react-router-dom";
import { Typography, Button, InputNumber } from "antd";
import {
  ExclamationCircleOutlined,
  LeftOutlined,
  LoadingOutlined,
  UploadOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addIntern,
  editIntern,
  getInternById,
} from "../../store/actions/internActions";
import { showError, showSuccess } from "../../utils/messages";
import api from "../../utils/api";
import serialize from "../../utils/serialize";
import {
  fetchUniDepartments,
  fetchUniversities,
} from "../../store/actions/universityActions";
import Avatar from "antd/lib/avatar/avatar";
import styles from "./NewOrEdit.module.scss";
import moment from "moment";
import CustomTopBarProgress from "../../components/CustomTopBarProgress/CustomTopBarProgress";
import { Editor } from "@toast-ui/react-editor";

const { Title } = Typography;

const EditIntern = (props) => {
  const location = useLocation();
  // const { mode } = location.state;
  const history = useHistory();
  const dispatch = useDispatch();
  const { interns, error } = useSelector((state) => state.intern);
  const { universities: uni, departments: uniDepartments } = useSelector(
    (state) => state.university
  );
  const { user } = useSelector((state) => state.user);
  const params = useParams();

  const emptyForm = {
    address: "",
    cv: "",
    date_of_birth: "",
    company_department_id: undefined,
    company_department: undefined,
    email: "",
    finished_date: "",
    phone_number: undefined,
    name: "",
    period_type: undefined,
    started_date: "",
    surname: "",
    work_type: undefined,
    university: {},
    university_id: undefined,
    university_department_id: undefined,
    university_department: {},
    user_id: user.id,
    identity_number: undefined,
    intern_opinion: {
      id: "",
      description: "",
      intern_id: "",
      user_id: "",
      created_at: "",
      updated_at: "",
    },
  };

  const emptyOpinionForm = {
    description: "",
    intern_id: params.id,
    user_id: user.id,
  };

  const emptyNoteForm = {
    description: "",
    intern_id: params.id,
    user_id: user.id,
  };

  const Mode = "new" | "edit" | "delete";

  const actionMode = "deactivate" | "addBlackList" | "removeBlackList";

  const [mode, setMode] = useState("new");

  const [newForm, setNewForm] = useState(emptyForm);
  const [loading, setLoading] = useState(false);
  const [isVisible, setIsVisible] = useState(false);

  //Following states for InternOpinion component
  const [noteForm, setNoteForm] = useState(emptyNoteForm);
  const [isNewNote, setIsNewNote] = useState(true);

  const [actionModeState, setActionModeState] = useState(actionMode); // For to learn deactivate or black list

  //Company departments state
  const [departments, setDepartments] = useState([]);

  const handleSubmit = async () => {
    if (mode === "new") dispatch(addIntern(newForm));
    if (mode === "edit") {
      dispatch(editIntern(params.id, newForm));

      if (isNewNote) addNote();
      if (!isNewNote) updateNote();
    }

    setNewForm(emptyForm);
    history.goBack();
  };

  const handleChange = (e) => {
    setNewForm({ ...newForm, [e.target.name]: e.target.value });
  };

  const showModal = (mode) => {
    setActionModeState(mode);
    setIsVisible(true);
  };

  const handleCancel = () => {
    setIsVisible(false);
    setActionModeState(actionMode);
  };

  const finishInternship = async () => {
    await api()
      .post(`/interns/${params.id}/update_status`, {
        status: 2,
        finished_date: new Date(),
      })
      .then((res) => {
        setNewForm(emptyForm);
        showSuccess("Kullanıcının stajı bitirildi");
        history.goBack();
      })
      .catch((err) => {
        setIsVisible(false);
        showError("Staj bitirme başarısız!");
      });
  };

  const addBlackList = async () => {
    await api()
      .post(`/interns/${params.id}/update_status`, {
        status: 3,
      })
      .then((res) => {
        setNewForm(emptyForm);
        showSuccess("Kullanıcı kara listeye eklendi");
        history.goBack();
      })
      .catch((err) => {
        setIsVisible(false);
        showError("Kara listeye ekleme başarısız!");
      });
  };

  const removeBlackList = async () => {
    await api()
      .post(`/interns/${params.id}/update_status`, {
        status: 2,
      })
      .then((res) => {
        setNewForm(emptyForm);
        showSuccess("Kullanıcı kara listeden çıkarıldı");
        history.goBack();
      })
      .catch((err) => {
        setIsVisible(false);
        showError("Kara listeden çıkarma başarısız!");
      });
  };

  const getInternById = async () => {
    setLoading(true);
    await api()
      .get(`/interns/${params.id}`)
      .then(({ data }) => {
        setNewForm(data.intern);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => setLoading(false));
  };

  // Note actions

  const editorRef = useRef();

  const handleNoteChange = (e) => {
    // setNoteForm({ ...noteForm, [e.target.name]: e.target.value });
    setNoteForm({
      ...newForm.intern_opinion,
      description: editorRef.current.getInstance().getMarkdown(),
    });
  };

  // const getNote = async () => {
  //   setLoading(true);
  //   await api()
  //     .get(`/interns/${params.id}/intern_opinions`)
  //     .then(({ data }) => {
  //       setNoteForm(data.intern_opinions);
  //       setIsNewNote(false);
  //       setLoading(false);
  //       // console.log(data.intern_opinions);
  //     })
  //     .catch((err) => {
  //       setIsNewNote(true);
  //       setLoading(false);
  //     });
  // };

  const addNote = async () => {
    //For intern_opinion table
    await api().post("/intern_opinions", noteForm);
  };

  const updateNote = async () => {
    await api().put(`/intern_opinions/${newForm.intern_opinion?.id}`, noteForm);
  };

  // Get company departments
  const getCompanyDepartments = async () => {
    // setLoading(true);
    await api()
      .get("/company_departments")
      .then(({ data }) => {
        setDepartments(data.company_departments);
        // setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        // setLoading(false);
      });
  };

  const fileSelectHandler = (e) => {
    setNewForm({ ...newForm, cv: e.target.files[0] });
  };

  useEffect(() => {
    if (params.id) {
      setMode("edit");
      getInternById();
    }
  }, [params.id]);

  useEffect(() => {
    if (newForm.intern_opinion?.description) {
      setIsNewNote(false);
    }
  }, [newForm.intern_opinion]);

  useEffect(() => {
    getCompanyDepartments();
    dispatch(fetchUniversities());
    dispatch(fetchUniDepartments());
    return () => {
      setNewForm(emptyForm);
    };
  }, []);

  return (
    <>
      {loading && <CustomTopBarProgress />}
      {loading ? (
        <div
          style={{
            height: "100vh",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Spin size="large" />
        </div>
      ) : (
        <div loading={loading} className={styles.card}>
          <Title
            style={{ textAlign: "center", marginBottom: "3rem" }}
            level={2}
          >
            {mode === "new" ? "Yeni Stajyer" : "Stajyeri Güncelle"}
          </Title>

          <Form onFinish={handleSubmit} className={styles.form_container}>
            <div className={styles.form_avatar_section}>
              <Form.Item className={styles.avatar}>
                <Avatar
                  size={150}
                  icon={<UserOutlined />}
                  className={styles.avatar_logo}
                />
              </Form.Item>
              <Form.Item label="Temel" className={styles.temel}>
                <Form.Item>
                  <Input
                    value={newForm.name}
                    className={styles.name_inputs}
                    name="name"
                    onChange={handleChange}
                    placeholder="Isim"
                  />
                </Form.Item>
                <Form.Item>
                  <Input
                    name="surname"
                    value={newForm.surname}
                    className={styles.name_inputs}
                    onChange={handleChange}
                    placeholder="Soyisim"
                  />
                </Form.Item>
                <Form.Item>
                  <DatePicker
                    name="date_of_birth"
                    value={
                      newForm.date_of_birth && moment(newForm.date_of_birth)
                    }
                    onChange={(date, dateString) =>
                      setNewForm({ ...newForm, date_of_birth: dateString })
                    }
                    placeholder="Doğum tarihi"
                    className={styles.date_of_birth}
                  />
                </Form.Item>
                <Form.Item className={styles.identity_wrapper}>
                  <Input
                    placeholder="TC kimlik no"
                    name="identity_number"
                    value={newForm.identity_number}
                    onChange={handleChange}
                    className={styles.identity}
                    type="number"
                  />
                </Form.Item>
              </Form.Item>
            </div>
            <Form.Item label="İletişim" className={styles.double_group}>
              <Form.Item className={styles.form_item}>
                <Input
                  name="email"
                  type="email"
                  placeholder="Email"
                  value={newForm.email}
                  onChange={handleChange}
                  className={styles.element_of_double}
                />
              </Form.Item>
              <Form.Item className={styles.form_item}>
                <Input
                  name="phone_number"
                  placeholder="Telefon numarası"
                  value={newForm.phone_number}
                  onChange={handleChange}
                  className={styles.element_of_double}
                />
              </Form.Item>
            </Form.Item>
            <Form.Item label="Dönem" className={styles.double_group}>
              <Form.Item className={styles.form_item}>
                <DatePicker
                  name="started_date"
                  placeholder="Başlangıç Tarihi"
                  value={newForm.started_date && moment(newForm.started_date)}
                  className={styles.element_of_double}
                  onChange={(date, dateString) =>
                    setNewForm({ ...newForm, started_date: dateString })
                  }
                />
              </Form.Item>
              <Form.Item className={styles.form_item}>
                <DatePicker
                  name="finished_date"
                  placeholder="Bitiş Tarih"
                  value={newForm.finished_date && moment(newForm.finished_date)}
                  onChange={(date, dateString) =>
                    setNewForm({ ...newForm, finished_date: dateString })
                  }
                  className={styles.element_of_double}
                />
              </Form.Item>
            </Form.Item>
            <Form.Item label="Adres" className={styles.long_input_wrapper}>
              <Input
                name="address"
                value={newForm.address}
                onChange={handleChange}
                className={styles.long_input}
              />
            </Form.Item>
            <Form.Item className={styles.additional_infos} label="Ek bilgiler">
              <Form.Item className={styles.info_item_wrapper}>
                <Select
                  allowClear
                  name="work_type"
                  placeholder="Çalışma Şekli"
                  onChange={(value) =>
                    setNewForm({ ...newForm, work_type: value })
                  }
                  defaultValue={newForm.work_type}
                  value={newForm.work_type}
                  className={styles.info_item}
                >
                  <Select.Option value={1}>Remote</Select.Option>
                  <Select.Option value={2}>Ofisten</Select.Option>
                </Select>
              </Form.Item>
              <Form.Item className={styles.info_item_wrapper}>
                <Select
                  allowClear
                  className="select"
                  onChange={(value) =>
                    setNewForm({ ...newForm, period_type: value })
                  }
                  placeholder="Staj Dönemi"
                  defaultValue={newForm.period_type}
                  value={newForm.period_type}
                  className={styles.info_item}
                >
                  <Select.Option value={1}>3 ay</Select.Option>
                  <Select.Option value={2}>6 ay</Select.Option>
                  <Select.Option value={3}>18 ay</Select.Option>
                </Select>
              </Form.Item>
              <Form.Item className={styles.info_item_wrapper}>
                <Select
                  allowClear
                  name="department"
                  value={newForm.company_department_id}
                  className={styles.info_item}
                  placeholder="Departman"
                  onChange={(value) =>
                    setNewForm({ ...newForm, company_department_id: value })
                  }
                >
                  {departments &&
                    departments.map((item) => (
                      <Select.Option key={item.id} value={item.id}>
                        {item.name}
                      </Select.Option>
                    ))}
                </Select>
              </Form.Item>
            </Form.Item>
            <Form.Item label="Üniversite" className={styles.uni_wrapper}>
              <Select
                value={newForm.university_id}
                placeholder="Üniversite seçiniz"
                onChange={(value) => {
                  setNewForm({ ...newForm, university_id: value });
                }}
                className={styles.uni_input}
              >
                {uni &&
                  uni.map((item) => (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  ))}
              </Select>
            </Form.Item>
            <Form.Item label="Bölüm" className={styles.long_input_wrapper}>
              <Select
                value={newForm.university_department_id}
                placeholder="Bölüm seçiniz"
                onChange={(value) => {
                  setNewForm({ ...newForm, university_department_id: value });
                }}
                className={styles.long_input}
              >
                {uniDepartments &&
                  uniDepartments.map((item) => (
                    <Select.Option key={item.id} value={item.id}>
                      {item.name}
                    </Select.Option>
                  ))}
              </Select>
            </Form.Item>
            <Form.Item label="CV yukle">
              <input type="file" onChange={fileSelectHandler} />
            </Form.Item>
            {mode === "edit" && (
              <div className={styles.md_editor}>
                <Form.Item label="Not">
                  <Editor
                    initialValue={newForm.intern_opinion?.description || ""}
                    previewStyle="tab"
                    height="300px"
                    initialEditType="markdown"
                    ref={editorRef}
                    onChange={handleNoteChange}
                  />
                </Form.Item>
              </div>
            )}
            <div className={styles.actions}>
              {newForm.status === "Active" && mode !== "new" && (
                <div
                  onClick={() => showModal("deactivate")}
                  className={styles.finish_internship}
                >
                  <p>Stajı bitir</p>
                </div>
              )}
              {newForm.status !== "Black List" && mode !== "new" && (
                <div
                  onClick={() => showModal("addBlackList")}
                  className={styles.black_list}
                >
                  <p>Kara Listeye Ekle</p>
                </div>
              )}
              {newForm.status === "Black List" && mode !== "new" && (
                <div
                  onClick={() => showModal("removeBlackList")}
                  className={styles.black_list}
                >
                  <p>Kara Listeden Çıkar</p>
                </div>
              )}
            </div>

            <Form.Item wrapperCol={{ offset: 10, span: 16 }}>
              <button className={styles.form_button}>
                {mode === "new" ? "Ekle" : "Güncelle"}
              </button>
            </Form.Item>
          </Form>
        </div>
      )}

      <Modal
        visible={isVisible}
        onCancel={handleCancel}
        okButtonProps={{ style: { borderRadius: "10px" } }}
        cancelButtonProps={{ style: { borderRadius: "10px" } }}
        onOk={
          actionModeState === "deactivate"
            ? finishInternship
            : actionModeState === "addBlackList"
            ? addBlackList
            : removeBlackList
        }
        okText={
          actionModeState === "deactivate"
            ? "Sonlandır"
            : actionModeState === "addBlackList"
            ? "Ekle"
            : "Evet"
        }
        cancelText="İptal et"
        title={
          actionModeState === "deactivate"
            ? "Stajı Bitir"
            : actionModeState === "addBlackList"
            ? "Kara Listeye Ekle"
            : "Kara Listeden Çıkar"
        }
        style={{ marginTop: "10rem" }}
      >
        {actionModeState === "deactivate" ? (
          <>
            Stajı sonlandırmak istediğinize emin misiniz? Bu işlem yapıldıktan
            sonra geri alınamaz.
          </>
        ) : actionModeState === "addBlackList" ? (
          <>Kara listeye eklemek istediğinize emin misiniz?</>
        ) : (
          <>Kara listeden çıkarmak istediğinize emin misiniz?</>
        )}
      </Modal>
    </>
  );
};

export default EditIntern;
