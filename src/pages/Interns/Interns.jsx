import React, { useRef, useState, useEffect } from "react";
import { Table, Tag, Space, Button, Form, Input, Select, Modal } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Link, Redirect, useHistory } from "react-router-dom";

import {
  addIntern,
  deleteIntern,
  fetchInterns,
} from "../../store/actions/internActions";
import api from "../../utils/api";
import EditIntern from "../NewOrEditIntern/NewOrEditIntern";
import { showError } from "../../utils/messages";
import SearchBar from "../../components/Search/SearchBar";
import CustomTopBarProgress from "../../components/CustomTopBarProgress/CustomTopBarProgress";

const emptyForm = {
  address: "",
  cv_url: "",
  date_of_birth: "",
  department: null,
  email: "",
  finished_date: "",
  phone_number: undefined,
  name: "",
  period_type: null,
  started_date: "",
  surname: "",
  work_type: null,
  university: {},
};

const Mode = "new" | "edit" | "delete";

const Uni = [
  {
    id: null,
    name: "",
    department: "",
  },
];

const Interns = () => {
  const dispatch = useDispatch();
  const { interns, loading, error } = useSelector((state) => state.intern);
  const history = useHistory();

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [form, setForm] = useState(emptyForm);
  const [mode, setMode] = useState(Mode);
  const [id, setId] = useState(null);
  const [internsData, setInternsData] = useState(interns);

  const handleOk = () => {
    if (mode === "delete") dispatch(deleteIntern(id));
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setForm(emptyForm);
  };

  const showModal = (mode) => {
    setIsModalVisible(true);
    setMode(mode);
  };

  useEffect(() => {
    dispatch(fetchInterns());
    setInternsData(interns);
  }, []);

  useEffect(() => {
    setInternsData(interns);
  }, [interns]);

  const columns = [
    {
      title: "Isim",
      dataIndex: "name",
      key: "name",
      render: (text, intern) => (
        <Link
          to={`/interns/intern_detail/${intern.id}`}
          style={{ color: "rgba(0, 0, 0, 0.85)", cursor: "pointer" }}
        >
          {text}
        </Link>
      ),
    },
    {
      title: "Soyisim",
      dataIndex: "surname",
      key: "surname",
    },
    {
      title: "Universite",
      dataIndex: "university",
      key: "university",
      render: (university) => `${university?.name}`,
    },
    {
      title: "Departman",
      dataIndex: "company_department",
      render: (company_department) => {
        let color = "blue";
        if (company_department.name === "Backend Developer") color = "blue";
        if (company_department.name === "Frontend Developer") color = "green";
        if (company_department.name === "Security") color = "orange";
        if (company_department.name === "Design") color = "pink";
        return <Tag color={color}>{company_department.name.toUpperCase()}</Tag>;
      },
      filters: [
        {
          text: "Backend Developer",
          value: "Backend Developer",
        },
        {
          text: "Frontend Developer",
          value: "Frontend Developer",
        },
        {
          text: "Security",
          value: "Security",
        },
        {
          text: "Design",
          value: "Design",
        },
      ],
      onFilter: (value, record) => record.company_department.name === value,
    },
    {
      title: "Durumu",
      dataIndex: "status",
      render: (status) => {
        if (status === "Active") {
          return <p style={{ color: "#00B035" }}>Active</p>;
        } else if (status === "Deactive") {
          return <p style={{ color: "#E52525" }}>Deactive</p>;
        } else {
          return <p style={{ color: "#262626" }}>Kara Liste</p>;
        }
      },
      filters: [
        {
          text: "Active",
          value: "Active",
        },
        {
          text: "Deactive",
          value: "Deactive",
        },
        {
          text: "Kara Liste",
          value: "Black List",
        },
      ],
      onFilter: (value, record) => record.status === value,
    },
    {
      title: "Başlangıç Tarihi",
      dataIndex: "started_date",
      key: "started_date",
      render: (started_date) => {
        return <>{new Date(started_date).toLocaleDateString()}</>;
      },
      sorter: (a, b) => new Date(a.started_date) - new Date(b.started_date),
    },
    {
      title: "Bitiş Tarihi",
      dataIndex: "finished_date",
      key: "finished_date",
      render: (finished_date) => {
        return <>{new Date(finished_date).toLocaleDateString()}</>;
      },
    },
    {
      title: "İşlem",
      key: "action",
      render: (
        text,
        intern // NOT: The first parameter is just that block, the second parameter is the whole row data.
      ) => (
        <Space>
          <EditOutlined
            style={{ color: "#40A9FF" }}
            onClick={() => {
              setForm(intern);
              setMode("edit");
              history.push({
                pathname: `/interns/edit/${intern.id}`,
                state: { mode: "edit" },
              });
            }}
          />
          <DeleteOutlined
            onClick={() => {
              showModal("delete");
              setId(intern.id);
            }}
            style={{ color: "#c70d00" }}
          />
        </Space>
      ),
    },
  ];

  return (
    <>
      <div>
        {loading && <CustomTopBarProgress />}
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            marginBottom: "0.8rem",
          }}
        >
          <Button
            type="primary"
            style={{ borderRadius: "10px" }}
            onClick={() => {
              setForm(emptyForm);
              setMode("new");
              history.push({
                pathname: "/interns/new",
                state: { mode: "new" },
              });
            }}
          >
            Yeni Stajyer
          </Button>
          <SearchBar
            data={interns}
            setData={setInternsData}
            placeholder="İsme göre ara.."
          />
        </div>
        <Modal
          title="Stajer silme"
          visible={isModalVisible}
          okButtonProps={{ style: { borderRadius: "10px" } }}
          cancelButtonProps={{ style: { borderRadius: "10px" } }}
          onOk={handleOk}
          onCancel={handleCancel}
          okText="Sil"
          cancelText="Vazgeç"
        >
          Bu stajeri silmeye emin misiniz?
        </Modal>
      </div>
      <Table
        loading={loading}
        columns={columns}
        dataSource={internsData} // For the search component
        rowKey="id"
      />
    </>
  );
};

export default Interns;
