import React, { useEffect } from "react";
import { Spin } from "antd";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { ImSpinner8 } from "react-icons/im";

import logo from "../../assets/images/logo1.png";
import styles from "./Login.module.scss";
import { login } from "../../store/actions/userActions";

const emptyCreds = {
  email: "",
  password: "",
};

const Login = () => {
  const dispatch = useDispatch();
  const {
    user,
    checkUserPending,
    loading,
    error: err,
  } = useSelector((state) => state.user);
  const history = useHistory();

  const [creds, setCreds] = useState(emptyCreds);
  const [error, setError] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(login(creds));
    // setCreds(emptyCreds)
  };

  const handleChange = (e) => {
    setError("");
    setCreds({ ...creds, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    setError(err);
  }, [err]);

  if (checkUserPending) {
    return (
      <div
        style={{
          height: "100vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spin size="large" />
      </div>
    );
  }

  return (
    <div className={styles.main}>
      <div className={styles.wallpaper} />
      <div className={styles.form}>
        <img src={logo} alt="Privia Security" className={styles.logo} />
        <form onSubmit={handleSubmit}>
          <h2>Sign In</h2>
          <div className={styles.field}>
            <input
              type="text"
              name="email"
              onChange={handleChange}
              value={creds.email}
              className={styles.input}
              placeholder="Email"
            />
          </div>
          <div className={styles.field}>
            <input
              type="password"
              name="password"
              onChange={handleChange}
              value={creds.password}
              className={styles.input}
              placeholder="Password"
            />
          </div>
          <div className={styles.button_wrapper}>
            <button
              type="submit"
              className={
                loading
                  ? `${styles.button} ${styles.button_loading}`
                  : styles.button
              }
            >
              {loading ? (
                <ImSpinner8 className={styles.spinner} size={20} />
              ) : (
                "Giriş yap"
              )}
            </button>
          </div>
        </form>
        <p style={{ color: "red" }}>{error && error}</p>
      </div>
    </div>
  );
};

export default Login;
