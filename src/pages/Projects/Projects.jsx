import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import {
  Button,
  Input,
  Space,
  Table,
  Form,
  DatePicker,
  Select,
  Tag,
} from "antd";
import Modal from "antd/lib/modal/Modal";
import moment from "moment";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";

import styles from "./Projects.module.scss";
import {
  addProject,
  deleteProject,
  editProject,
  fetchProjects,
} from "../../store/actions/projectActions";
import { fetchInterns } from "../../store/actions/internActions";
import { fetchUsers } from "../../store/actions/userActions";
import SearchBar from "../../components/Search/SearchBar";
import CustomTopBarProgress from "../../components/CustomTopBarProgress/CustomTopBarProgress";

const emptyForm = {
  project_name: "",
  started_date: "",
  finished_date: "",
  description: "",
  user_ids: [],
  intern_ids: [],
};

const Mode = "new" | "edit" | "delete";

const Projects = () => {
  const dispatch = useDispatch();
  const {
    allProjects: data,
    loading,
    error,
  } = useSelector((state) => state.project);
  const { interns, loading: internLoading } = useSelector(
    (state) => state.intern
  );
  const { allUsers: users } = useSelector((state) => state.user);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [newProject, setNewProject] = useState(emptyForm);
  const [mode, setMode] = useState(Mode);
  const [id, setId] = useState(undefined);
  const [projectsData, setProjectsData] = useState(data);

  //For project detail modal
  const [detailModalVisible, setDetailModalVisible] = useState(false);

  // Following methods for project detail modal
  const showProjetcDetail = () => {
    setDetailModalVisible(true);
  };

  const detailModalOk = () => {
    setDetailModalVisible(false);
  };

  const detailModalCancel = () => {
    setDetailModalVisible(false);
  };

  // Following methods for new or update or delete modal
  const showModal = () => {
    setIsModalVisible(true);
    dispatch(fetchInterns());
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setNewProject(emptyForm);
  };

  const handleOk = () => {
    if (mode === "new") dispatch(addProject(newProject));
    if (mode === "edit") dispatch(editProject(id, newProject));
    if (mode === "delete") dispatch(deleteProject(id));
    setIsModalVisible(false);
    setNewProject(emptyForm);
  };

  const handleChange = (e) => {
    setNewProject({ ...newProject, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    dispatch(fetchProjects());
    dispatch(fetchInterns());
    dispatch(fetchUsers());
  }, []);

  useEffect(() => {
    setProjectsData(data);
  }, [data]);

  const columns = [
    {
      title: "Proje Adı",
      dataIndex: "project_name",
      key: "project_name",
      render: (name, project) => (
        <Link
          to={`/projects/project_detail/${project.id}`}
          style={{ color: "rgba(0, 0, 0, 0.85)" }}
        >
          {name}
        </Link>
      ),
    },
    {
      title: "Proje Mentörü",
      dataIndex: "users",
      key: "users",
      render: (users) => (
        <>
          {users.map((user) => {
            return (
              <Tag key={user.id}>
                <Link to={`/projects/user_detail/${user.id}`}>
                  {user.name.toUpperCase()} {user.surname.toUpperCase()}
                </Link>
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: "Stajyer",
      dataIndex: "interns",
      key: "interns",
      render: (users) => (
        <>
          {users.map((intern) => {
            return (
              <Tag key={intern.id}>
                <Link to={`/projects/intern_detail/${intern.id}`}>
                  {intern.name.toUpperCase()} {intern.surname.toUpperCase()}
                </Link>
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: "Başlangıç Tarihi",
      dataIndex: "started_date",
      key: "started_date",
      render: (started_date) => {
        return <>{new Date(started_date).toLocaleDateString()}</>;
      },
      sorter: (a, b) => new Date(a.started_date) - new Date(b.started_date),
    },
    {
      title: "İşlem",
      key: "action",
      render: (text, project) => (
        <Space>
          <EditOutlined
            style={{ color: "#40A9FF" }}
            onClick={() => {
              setMode("edit");
              setId(project.id);
              setNewProject(project);
              showModal();
            }}
          />
          <DeleteOutlined
            style={{ color: "#c70d00" }}
            onClick={() => {
              setMode("delete");
              setId(project.id);
              showModal();
            }}
          />
        </Space>
      ),
    },
  ];

  return (
    <div>
      {loading && <CustomTopBarProgress />}
      <div className={styles.button_wrapper}>
        <Button
          type="primary"
          style={{ borderRadius: "10px" }}
          onClick={() => {
            setMode("new");
            showModal();
          }}
        >
          Yeni Proje
        </Button>
        <SearchBar
          data={data}
          setData={setProjectsData}
          placeholder="Projeye göre ara.."
        />
      </div>
      <Modal
        visible={isModalVisible}
        onCancel={handleCancel}
        okButtonProps={{ style: { borderRadius: "10px" } }}
        cancelButtonProps={{ style: { borderRadius: "10px" } }}
        onOk={handleOk}
        okText={
          mode === "new" ? "Oluştur" : mode === "edit" ? "Güncelle" : "Sil"
        }
        cancelText="Vazgeç"
        className={styles.modal}
        width={600}
        title={
          mode === "new"
            ? "Proje Oluştur"
            : mode === "edit"
            ? "Projeyi Güncelle"
            : "Projeyi Sil"
        }
      >
        {mode !== "delete" ? (
          <Form
            initialValues={newProject}
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 17 }}
          >
            <Form.Item label="Proje Adı">
              <Input
                style={{ borderRadius: "10px" }}
                value={newProject.project_name}
                name="project_name"
                onChange={handleChange}
              />
            </Form.Item>
            <Form.Item label="Süre" style={{ marginBottom: "0" }}>
              <Form.Item style={{ display: "inline-block" }}>
                <DatePicker
                  className={styles.date_picker}
                  placeholder="Başlangıç Tarihi"
                  value={
                    newProject.started_date && moment(newProject.started_date)
                  }
                  onChange={(date, dateString) =>
                    setNewProject({ ...newProject, started_date: dateString })
                  }
                />
              </Form.Item>
              <Form.Item style={{ display: "inline-block" }}>
                <DatePicker
                  className={styles.date_picker}
                  placeholder="Bitiş Tarih"
                  value={
                    newProject.finished_date && moment(newProject.finished_date)
                  }
                  onChange={(date, dateString) =>
                    setNewProject({ ...newProject, finished_date: dateString })
                  }
                />
              </Form.Item>
            </Form.Item>
            <Form.Item label="Stajyerler">
              <Select
                mode="multiple"
                loading={internLoading}
                value={newProject.intern_ids}
                onChange={(values) =>
                  setNewProject({ ...newProject, intern_ids: values })
                }
              >
                {interns &&
                  interns.map((intern) => {
                    if (intern.status) {
                      return (
                        <Select.Option key={intern.id} value={intern.id}>
                          {intern.name} {intern.surname}
                        </Select.Option>
                      );
                    }
                  })}
              </Select>
            </Form.Item>
            <Form.Item label="Mentörler">
              <Select
                value={newProject.user_ids}
                mode="multiple"
                onChange={(values) =>
                  setNewProject({ ...newProject, user_ids: values })
                }
              >
                {users.map((user) => {
                  return (
                    <Select.Option key={user.id} value={user.id}>
                      {user.name} {user.surname}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>
            <Form.Item label="Proje Açıklaması">
              <Input.TextArea
                value={newProject.description}
                onChange={handleChange}
                name="description"
                className={styles.text_area}
              />
            </Form.Item>
          </Form>
        ) : (
          <>Projeyi silmeye emin misiniz?</>
        )}
      </Modal>

      <Table columns={columns} dataSource={projectsData} rowKey="id" />
    </div>
  );
};

export default Projects;
