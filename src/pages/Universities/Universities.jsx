import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button, Input, Space, Table, Form, Modal } from "antd";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import {
  addUniDepartment,
  addUniversity,
  deleteUniDepartment,
  deleteUniversity,
  editUniDepart,
  editUniDepartment,
  editUniversity,
  fetchUniDepartments,
  fetchUniversities,
} from "../../store/actions/universityActions";
import { showError } from "../../utils/messages";
import SearchBar from "../../components/Search/SearchBar";
import CustomTopBarProgress from "../../components/CustomTopBarProgress/CustomTopBarProgress";
import styles from "./Universities.module.scss";

const emptyForm = {
  name: "",
};

const Mode = "new" | "edit" | "delete";

const Universities = () => {
  const { universities, departments, loading, error } = useSelector(
    (state) => state.university
  );
  const dispatch = useDispatch();
  const history = useHistory();

  const [isUniModalVisible, setIsUniModalVisible] = useState(false);
  const [isDepartModalVisible, setIsDepartModalVisible] = useState(false);
  const [uniForm, setUniForm] = useState(emptyForm);
  const [departForm, setDepartForm] = useState(emptyForm);
  const [uniMode, setUniMode] = useState(Mode);
  const [departMode, setDepartMode] = useState(Mode);
  const [id, setId] = useState(null);
  const [uniData, setUniData] = useState(universities);
  const [uniDeparts, setUniDeparts] = useState(departments);

  const showUniModal = (mode) => {
    setUniMode(mode);
    setIsUniModalVisible(true);
  };
  const showDepartModal = (mode) => {
    setDepartMode(mode);
    setIsDepartModalVisible(true);
  };

  const handleOk = () => {
    setIsUniModalVisible(false);
    if (uniMode === "new") dispatch(addUniversity(uniForm));
    else if (uniMode === "delete") dispatch(deleteUniversity(id));
    else if (uniMode === "edit") dispatch(editUniversity(id, uniForm));
    setUniMode("new");
    setId(null);
    setUniForm(emptyForm);
  };

  const handleDepartModalOk = () => {
    setIsDepartModalVisible(false);
    if (departMode === "new") dispatch(addUniDepartment(departForm));
    if (departMode === "edit") dispatch(editUniDepartment(id, departForm));
    if (departMode === "delete") dispatch(deleteUniDepartment(id));
  };

  const handleUniModalCancel = () => {
    setIsUniModalVisible(false);
    setUniForm(emptyForm);
  };

  const handleDepartModalCancel = () => {
    setIsDepartModalVisible(false);
    setDepartForm(emptyForm);
  };

  const uniColumns = [
    {
      title: "Üniversite",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "İşlem",
      key: "action",
      render: (university) => (
        <Space>
          <EditOutlined
            style={{ color: "#40A9FF" }}
            onClick={() => {
              showUniModal("edit");
              setUniForm(university);
              setId(university.id);
            }}
          />
          <DeleteOutlined
            style={{ color: "#c70d00" }}
            onClick={() => {
              showUniModal("delete");
              setId(university.id);
            }}
          />
        </Space>
      ),
    },
  ];

  const departmentColumns = [
    {
      title: "Bölüm",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "İşlem",
      key: "action",
      render: (department) => (
        <Space>
          <EditOutlined
            style={{ color: "#40A9FF" }}
            onClick={() => {
              showDepartModal("edit");
              setDepartForm(department);
              setId(department.id);
            }}
          />
          <DeleteOutlined
            style={{ color: "#c70d00" }}
            onClick={() => {
              showDepartModal("delete");
              setId(department.id);
            }}
          />
        </Space>
      ),
    },
  ];

  useEffect(() => {
    dispatch(fetchUniversities());
    dispatch(fetchUniDepartments());
  }, []);

  useEffect(() => {
    setUniData(universities);
    setUniDeparts(departments);
  }, [universities, departments]);

  return (
    <>
      {loading && <CustomTopBarProgress />}
      <div className={styles.container}>
        <div className={styles.universities_section}>
          <h3>Üniversiteler</h3>
          <div className={styles.button_search}>
            <Button
              className={styles.button}
              onClick={() => showUniModal("new")}
              type="primary"
            >
              Yeni Üniversite
            </Button>
            <SearchBar
              data={universities}
              setData={setUniData}
              placeholder="Üniversiteye göre ara.."
            />
          </div>
          <Table
            loading={loading}
            columns={uniColumns}
            dataSource={uniData} // For the search component
            rowKey="id"
          />
        </div>
        <div className={styles.departments_section}>
          <h3>Bölümler</h3>
          <div className={styles.button_search}>
            <Button
              className={styles.button}
              onClick={() => showDepartModal("new")}
              type="primary"
            >
              Yeni Bölüm
            </Button>
            <SearchBar
              data={departments}
              setData={setUniDeparts}
              placeholder="Bölüme göre ara.."
            />
          </div>
          <Table
            loading={loading}
            columns={departmentColumns}
            dataSource={uniDeparts} // For the search component
            rowKey="id"
          />
        </div>
      </div>
      <Modal
        title={
          uniMode === "new"
            ? "Üniversite Ekle"
            : uniMode === "edit"
            ? "Üniversiteyi Güncelle"
            : uniMode === "delete"
            ? "Üniversiteyi sil"
            : null
        }
        visible={isUniModalVisible}
        okButtonProps={{ style: { borderRadius: "10px" } }}
        cancelButtonProps={{ style: { borderRadius: "10px" } }}
        onOk={handleOk}
        onCancel={handleUniModalCancel}
        okText={
          uniMode === "new"
            ? "Oluştur"
            : uniMode === "edit"
            ? "Güncelle"
            : "Sil"
        }
        cancelText="Vazgeç"
      >
        {uniMode === "new" || uniMode === "edit" ? (
          <Form labelCol={{ span: 5 }} wrapperCol={{ span: 18 }}>
            <Form.Item label="Üniversite">
              <Input
                name="name"
                value={uniForm.name}
                onChange={(e) =>
                  setUniForm({ ...uniForm, name: e.target.value })
                }
                className={styles.input}
                style={{ borderRadius: "10px" }}
              />
            </Form.Item>
          </Form>
        ) : uniMode === "delete" ? (
          <>Bu üniversiteyi silmeye emin misiniz?</>
        ) : null}
      </Modal>

      <Modal
        title={
          departMode === "new"
            ? "Bölüm Ekle"
            : departMode === "edit"
            ? "Bölümü Güncelle"
            : departMode === "delete"
            ? "Bölümü sil"
            : null
        }
        visible={isDepartModalVisible}
        okButtonProps={{ style: { borderRadius: "10px" } }}
        cancelButtonProps={{ style: { borderRadius: "10px" } }}
        onOk={handleDepartModalOk}
        onCancel={handleDepartModalCancel}
        okText={
          departMode === "new"
            ? "Oluştur"
            : departMode === "edit"
            ? "Güncelle"
            : "Sil"
        }
        cancelText="Vazgeç"
      >
        {departMode === "new" || departMode === "edit" ? (
          <Form labelCol={{ span: 5 }} wrapperCol={{ span: 18 }}>
            <Form.Item label="Bölüm">
              <Input
                name="name"
                value={departForm.name}
                onChange={(e) =>
                  setDepartForm({ ...departForm, name: e.target.value })
                }
                className={styles.input}
                style={{ borderRadius: "10px" }}
              />
            </Form.Item>
          </Form>
        ) : departMode === "delete" ? (
          <>Bu bölümü silmeye emin misiniz?</>
        ) : null}
      </Modal>
    </>
  );
};

export default Universities;
