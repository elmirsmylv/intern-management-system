import { Card, Col, Row, Tag } from "antd";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { useLocation, useParams } from "react-router";

import styles from "./UserDetail.module.scss";
import { getSingleUser } from "../../store/actions/userActions";
import ProfileCard from "../../components/ProfileCard/ProfileCard";
import CustomTopBarProgress from "../../components/CustomTopBarProgress/CustomTopBarProgress";

const UserDetail = () => {
  const params = useParams();
  const location = useLocation();
  const dispatch = useDispatch();
  const { user, loading, error } = useSelector((state) => state.user);

  useEffect(() => {
    dispatch(getSingleUser(params.id));
  }, []);

  let color = "blue";
  if (
    user.company_department &&
    user.company_department.name === "Backend Developer"
  )
    color = "blue";
  if (
    user.company_department &&
    user.company_department.name === "Frontend Developer"
  )
    color = "green";
  if (user.company_department && user.company_department.name === "Security")
    color = "orange";
  if (user.company_department && user.company_department.name === "Design")
    color = "pink";

  const url = location.pathname.startsWith("/user_detail") ? true : false;

  console.log(user);

  return (
    <div className={styles.container}>
      {loading && <CustomTopBarProgress />}
      <div className={styles.user_detail}>
        <Card className={styles.card}>
          <div className={styles.avatar}></div>
          <div className={styles.name_section}>
            <h3>
              {user.name} {user.surname}
            </h3>
            <p>{user.department}</p>
          </div>
          <div className={styles.tag_section}>
            <Tag className={styles.tag} color={color}>
              {user.company_department &&
                user.company_department.name.toUpperCase()}
            </Tag>
          </div>
          <div className={styles.email_section}>
            <p>{user.email}</p>
          </div>
        </Card>
      </div>
      <div className={styles.interns}>
        {user.interns?.map((intern) => {
          return (
            <div className={styles.intern}>
              <ProfileCard
                url={url}
                width={305}
                data={intern}
                loading={loading}
                isNoteVisible={false}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default UserDetail;
