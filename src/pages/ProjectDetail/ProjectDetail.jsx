import {
  Card,
  Progress,
  Table,
  Form,
  Input,
  DatePicker,
  Modal,
  Select,
} from "antd";
import React, { useEffect, useState } from "react";
import { AiOutlineFundProjectionScreen } from "react-icons/ai";
import { EditOutlined } from "@ant-design/icons";
import moment from "moment";
import { useParams } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";

import api from "../../utils/api";
import styles from "../Projects/Projects.module.scss";
import { calculatePercentage } from "../../utils/calculatePercentage";
import { editProject, getProjectById } from "../../store/actions/projectActions";
import { fetchInterns } from "../../store/actions/internActions";
import { fetchUsers } from "../../store/actions/userActions";
import CustomTopBarProgress from "../../components/CustomTopBarProgress/CustomTopBarProgress";

const ProjectDetail = () => {
  const columns = [
    {
      title: "Stajyer Adı",
      dataIndex: "name",
      key: "name",
      render: (name, intern) => (
        <>
          {name} {intern.surname}
        </>
      ),
    },
    {
      title: "Başlangıç Tarihi",
      dataIndex: "started_date",
      key: "started_date",
    },
    {
      title: "Bitiş Tarihi",
      dataIndex: "finished_date",
      key: "finished_date",
    },
    {
      title: "İşlemler",
      key: "action",
      render: (text, intern) => (
        <Link to={`/projects/project_detail/intern_detail/${intern.id}`}>
          <button className={styles.intern_detail_button}>Detayı Gör</button>
        </Link>
      ),
    },
  ];

  const params = useParams();
  const dispatch = useDispatch();
  const { allProjects, projectById, loading } = useSelector(
    (state) => state.project
  );
  const { interns, loading: internLoading } = useSelector(
    (state) => state.intern
  );
  const { allUsers: users } = useSelector((state) => state.user);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [newProject, setNewProject] = useState({});

  const handleChange = (e) => {
    setNewProject({ ...newProject, [e.target.name]: e.target.value });
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleOk = () => {
    dispatch(editProject(params.id, newProject));
    setIsModalVisible(false);
  };

  const getProject = () => {
    dispatch(getProjectById(params.id));
    setNewProject(projectById);
  };

  useEffect(() => {
    getProject();
    dispatch(fetchInterns());
    dispatch(fetchUsers());

    return () => {
      setNewProject({});
    };
  }, []);

  useEffect(() => {
    dispatch(getProjectById(params.id));
  }, [allProjects]);

  useEffect(() => {
    setNewProject(projectById); // if you delete it newProject state not work correctly
  }, [projectById]);

  const isFinish =
    new Date(projectById.finished_date) < new Date() ? false : true;

  return (
    <>
      {loading && <CustomTopBarProgress />}
      <div className={styles.container}>
        <Card className={styles.detail_card}>
          <div className={styles.name_section}>
            <div
              className={`${styles.icon} ${
                isFinish ? styles.active : styles.deactive
              }`}
            >
              <AiOutlineFundProjectionScreen size={22} />
            </div>
            <h4>{projectById.project_name}</h4>
          </div>
          <div className={styles.content}>
            <div className={styles.content_item}>
              <p>Başlangıç Tarihi: </p>
              <p>{projectById.started_date}</p>
            </div>
            <div className={styles.content_item}>
              <p>Bitiş Tarihi: </p>
              <p>{projectById.finished_date}</p>
            </div>
          </div>
          <div
            className={styles.edit_button}
            onClick={() => setIsModalVisible(true)}
          >
            <EditOutlined />
          </div>
        </Card>
        <div className={styles.right_side}>
          <Card className={styles.progress_card}>
            <div className={styles.card_top}>
              <p className={styles.card_title}>Proje Detayı</p>
            </div>
            <div className={styles.project}>
              <div className={styles.project_titles}>
                <div className={styles.project_user}>
                  <p>Proje Mentörü </p>
                  {projectById.users &&
                    projectById.users.map((user) => (
                      <span key={user.id}>
                        {user.name} {user.surname}{" "}
                      </span>
                    ))}
                </div>
                <div className={styles.project_description}>
                  <p>Proje Aciklamasi</p>
                  <span className={styles.description_content}>
                    {projectById.description}
                  </span>
                </div>
              </div>
              <Progress
                strokeColor={{
                  from: "#108ee9",
                  to: "#87d068",
                }}
                percent={calculatePercentage(
                  projectById.started_date,
                  projectById.finished_date
                )}
                status={
                  calculatePercentage(
                    projectById.started_date,
                    projectById.finished_date
                  ) >= 100
                    ? "success"
                    : "active"
                }
                className={styles.progressBar}
              />
            </div>
          </Card>
          <Card className={styles.assigned_interns_card}>
            <h4>Atanan Stajyerler</h4>
            <Table
              columns={columns}
              dataSource={projectById && projectById.interns}
              rowKey="id"
            />
          </Card>
        </div>
      </div>
      {/* UPDATE MODAL */}
      <Modal
        visible={isModalVisible}
        onCancel={handleCancel}
        okButtonProps={{ style: { borderRadius: "10px" } }}
        cancelButtonProps={{ style: { borderRadius: "10px" } }}
        onOk={handleOk}
        okText="Güncelle"
        cancelText="Vazgeç"
        className={styles.modal}
        width={600}
        title="Projeyi Güncelle"
      >
        <Form labelCol={{ span: 5 }} wrapperCol={{ span: 17 }}>
          <Form.Item label="Proje Adı">
            <Input
              style={{ borderRadius: "10px" }}
              value={newProject.project_name}
              name="project_name"
              onChange={handleChange}
            />
          </Form.Item>
          <Form.Item label="Süre" style={{ marginBottom: "0" }}>
            <Form.Item style={{ display: "inline-block" }}>
              <DatePicker
                className={styles.date_picker}
                placeholder="Başlangıç Tarihi"
                value={
                  newProject.started_date && moment(newProject.started_date)
                }
                onChange={(date, dateString) =>
                  setNewProject({ ...newProject, started_date: dateString })
                }
              />
            </Form.Item>
            <Form.Item style={{ display: "inline-block" }}>
              <DatePicker
                className={styles.date_picker}
                placeholder="Bitiş Tarih"
                value={
                  newProject.finished_date && moment(newProject.finished_date)
                }
                onChange={(date, dateString) =>
                  setNewProject({ ...newProject, finished_date: dateString })
                }
              />
            </Form.Item>
          </Form.Item>
          <Form.Item label="Stajyerler">
            <Select
              mode="multiple"
              loading={internLoading}
              value={newProject.intern_ids}
              onChange={(values) =>
                setNewProject({ ...newProject, intern_ids: values })
              }
            >
              {interns.map((intern) => {
                if (intern.status) {
                  return (
                    <Select.Option key={intern.id} value={intern.id}>
                      {intern.name} {intern.surname}
                    </Select.Option>
                  );
                }
              })}
            </Select>
          </Form.Item>
          <Form.Item label="Mentörler">
            <Select
              value={newProject.user_ids}
              mode="multiple"
              onChange={(values) =>
                setNewProject({ ...newProject, user_ids: values })
              }
            >
              {users.map((user) => {
                return (
                  <Select.Option key={user.id} value={user.id}>
                    {user.name} {user.surname}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item label="Proje Açıklaması">
            <Input.TextArea
              value={newProject.description}
              onChange={handleChange}
              name="description"
              className={styles.text_area}
            />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default ProjectDetail;
