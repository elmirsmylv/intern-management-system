import { Button, Card, Input } from "antd";
import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation, useParams } from "react-router-dom";

import style from "./InternDetail.module.scss";
import api from "../../utils/api";
import ProjectProgresses from "../../components/ProjectProgresses/ProjectProgresses";
import CriteriaCard from "../../components/CriteriaCard/CriteriaCard";
import ProfileCard from "../../components/ProfileCard/ProfileCard";
import CustomTopBarProgress from "../../components/CustomTopBarProgress/CustomTopBarProgress";
import Projects from "../Projects/Projects";

const { TextArea } = Input;

const emptyForm = {
  id: undefined,
  address: "",
  cv_url: "",
  date_of_birth: "",
  department: "",
  email: "",
  finished_date: "",
  phone_number: undefined,
  name: "",
  score: undefined,
  period_type: null,
  started_date: "",
  surname: "",
  work_type: null,
  university: {},
  qr_code: "",
  uuid: "",
};

const InternDetail = () => {
  const location = useLocation();
  const history = useHistory();
  const params = useParams();
  const dispatch = useDispatch();

  // Intern states
  const [intern, setIntern] = useState(emptyForm);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");

  //QR code states
  const [qrcodePending, setQrcodePending] = useState(false);

  const getInternById = async () => {
    setLoading(true);
    setError("");

    await api()
      .get(`/interns/${params.id}`)
      .then((res) => {
        setIntern(res.data.intern);
        console.log(res.data.intern); // TODO delete
      })
      .catch((err) => setError(err))
      .finally(() => setLoading(false));
  };

  const createQRCode = async () => {
    setQrcodePending(true);
    setError("");
    try {
      const { data } = await api().post("/interns_qr_code", {
        intern_id: intern.id || null,
      });
      setQrcodePending(false);
      console.log(data);
      getInternById();
    } catch (error) {
      setQrcodePending(false);
      console.log(error);
    }
  };

  useEffect(() => {
    getInternById();
  }, []);

  let color = "blue";
  if (intern.department === "backend developer") color = "blue";
  if (intern.department === "frontend developer") color = "green";
  if (intern.department === "security") color = "orange";
  if (intern.department === "designer") color = "pink";

  return (
    <>
      {loading && <CustomTopBarProgress />}
      <div className={style.container}>
        <div className={style.left}>
          <ProfileCard data={intern} loading={loading} isNoteVisible={true} />
          <div className={style.qrcode_section}>
            {intern.uuid ? (
              <Card className={style.qrcode_card}>
                <div className={style.id_section}>
                  <span className={style.id_title}>ID numarası:</span>
                  <span className={style.id}>{intern.uuid}</span>
                </div>
                <img src={intern.qr_code} alt="" width="90%" />
              </Card>
            ) : loading ? null : (
              <Button
                type="primary"
                className={style.button}
                onClick={createQRCode}
              >
                QR kod oluştur
              </Button>
            )}
          </div>
        </div>
        <div className={style.right}>
          <div className={style.progresses}>
            <ProjectProgresses internId={params.id} />
          </div>
          <div className={style.all_projects}>
            <Card style={{ minHeight: "302px" }}>
              <Projects />
            </Card>
          </div>
          <div className={style.criteria}>
            <CriteriaCard />
          </div>
        </div>
      </div>
    </>
  );
};

export default InternDetail;
