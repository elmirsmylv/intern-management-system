import React, { useEffect, useRef, useState } from "react";
import { ImSpinner8 } from "react-icons/im";
import { Avatar, Card, Tag } from "antd";
import { useHistory, useLocation, useParams } from "react-router";

import styles from "./Verification.module.scss";
import Logo from "../../assets/images/Logo.svg";
import api from "../../utils/api";
import { showError } from "../../utils/messages";
import logo from "../../assets/images/logo1.png";
import star from "../../assets/images/star.svg";
import wallpaper from "../../assets/images/verificationWallpaper.jpg";

const VerificationType = "identity" | "uuid";

const EmptyIdentityForm = {
  identity_number: "",
  date_of_birth: "",
};

const EmptyUuidForm = {
  uuid: "",
};

const emptyInternState = {
  address: "",
  date_of_birth: "",
  department: null,
  email: "",
  finished_date: "",
  phone_number: undefined,
  name: "",
  period_type: null,
  started_date: "",
  surname: "",
  work_type: null,
  university: {},
  university_department: {},
  university_department_id: "",
  university_id: "",
  company_department: {},
  company_department_id: undefined,
  cv: "",
  id: undefined,
  intern_opinion: {},
  intern_criteria: [],
  projects: [],
  score: {},
  status: "",
  user: {},
  uuid: "",
};

const Verification = () => {
  const location = useLocation();
  const history = useHistory();

  const [verificationType, setVerificationType] = useState("identity");
  const [identityForm, setIdentityForm] = useState(EmptyIdentityForm);
  const [uuid, setUuid] = useState("");
  const [loading, setLoading] = useState(false);

  const [verifiedIntern, setVerifiedIntern] = useState(emptyInternState);

  const verifyWithIdentity = async (e) => {
    e.preventDefault();
    setLoading(true);

    await api()
      .post("/intern_verification/intern_verify", identityForm)
      .then(({ data }) => {
        setVerifiedIntern(data.intern);
        console.log(data.intern);
      })
      .catch((err) => showError(err.response.data.message))
      .finally(() => {
        setLoading(false);
        setIdentityForm(EmptyIdentityForm);
      });
  };

  const verifyWithUuid = async (data) => {
    setLoading(true);
    await api()
      .post("/intern_verification/intern_verify_with_uuid", data)
      .then(({ data }) => {
        console.log(data.intern); // TODO delete
        setVerifiedIntern(data.intern);
      })
      .catch((err) => {
        err.response.data.message && showError(err.response.data.message);
      })
      .finally(() => {
        setLoading(false);
        setUuid("");
        return false;
      });
  };

  const handleChange = (e) => {
    setIdentityForm({ ...identityForm, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    if (location.search) {
      const searhUuid = location.search.split("=")[1];
      verifyWithUuid({ uuid: searhUuid });
    }
  }, []);

  useEffect(() => {
    console.log("verif", verifiedIntern);
  }, [verifiedIntern]);

  return (
    <>
      {verifiedIntern.name ? (
        <div className={styles.intern_container}>
          <div className={styles.content}>
            <div
              className={styles.logo_section}
              onClick={() => {
                setVerifiedIntern(emptyInternState);
                history.push("/intern_verification");
              }}
            >
              <img src={logo} alt="Privia Security" />
            </div>
            <div className={styles.intern_detail}>
              <Card>
                <div className={styles.intern_detail_card}>
                  <div className={styles.card_left}>
                    <Avatar size={50} />
                    <div className={styles.info}>
                      <h2>
                        {verifiedIntern.name} {verifiedIntern.surname}
                      </h2>
                      <span>Stajyer</span>
                      <Tag color="pink" className={styles.tag}>
                        {verifiedIntern.company_department.name || ""}
                      </Tag>
                    </div>
                  </div>
                  <div className={styles.personal_info}>
                    <div className={styles.field}>
                      <h4 className={styles.title}>TC Kimlik Numarası:</h4>
                      <span className={styles.desc}>
                        {verifiedIntern.identity_number || "-"}
                      </span>
                    </div>
                    <div className={styles.field}>
                      <h4 className={styles.title}>ID Numarası:</h4>
                      <span className={styles.desc}>
                        {verifiedIntern.uuid || "-"}
                      </span>
                    </div>
                  </div>
                  <div className={styles.general_info}>
                    <div className={styles.field}>
                      <h4 className={styles.title}>Staj Başlangıç Tarihi:</h4>
                      <span className={styles.desc}>
                        {verifiedIntern.started_date}
                      </span>
                    </div>
                    <div className={styles.field}>
                      <h4 className={styles.title}>Staj Bitiş Tarihi:</h4>
                      <span className={styles.desc}>
                        {verifiedIntern.started_date || "-"}
                      </span>
                    </div>
                    <div className={styles.field}>
                      <h4 className={styles.title}>Rating:</h4>
                      <span className={styles.desc}>
                        {verifiedIntern.score} / 10
                      </span>
                    </div>
                  </div>
                </div>
              </Card>
            </div>
            <div className={styles.intern_projects}>
              <Card>
                <div className={styles.projects_container}>
                  <h2 className={styles.card_title}>Katıldığı Projeler</h2>
                  {verifiedIntern.projects?.map((pr) => (
                    <div key={pr.id} className={styles.project}>
                      <div className={styles.project_field}>
                        <h4 className={styles.title}>Proje Adı </h4>
                        <span className={styles.desc}>
                          {pr.project.project_name}
                        </span>
                      </div>
                      <div className={styles.project_field}>
                        <h4 className={styles.title}>Proje Mentörü </h4>
                        <span className={styles.desc}>
                          {pr.users?.map(
                            (user) => user.name + " " + user.surname + " "
                          )}
                        </span>
                      </div>
                      <div className={styles.project_field}>
                        <h4 className={styles.title}>
                          Proje Başlangıç Tarihi{" "}
                        </h4>
                        <span className={styles.desc}>
                          {pr.project.started_date}
                        </span>
                      </div>
                      <div className={styles.project_field}>
                        <h4 className={styles.title}>Proje Bitiş Tarihi </h4>
                        <span className={styles.desc}>
                          {pr.project.finished_date || "-"}
                        </span>
                      </div>
                      <div className={styles.project_field}>
                        <h4 className={styles.title}>Proje Açıklaması </h4>
                        <span className={styles.desc}>
                          {pr.project.description}
                        </span>
                      </div>
                    </div>
                  ))}
                </div>
              </Card>
            </div>
            <div className={styles.intern_criterias}>
              <Card>
                <div className={styles.criterias_container}>
                  <h2 className={styles.card_title}>Kriterler</h2>
                  <div className={styles.criterias}>
                    {verifiedIntern?.intern_criteria?.map((cr) => (
                      <div className={styles.criteria}>
                        <div className={styles.criteria_content}>
                          <h4 className={styles.title}>{cr.name}</h4>
                          <div className={styles.score}>
                            <img src={star} alt="Star" />
                            <span>{cr.score.toFixed(1)}</span>
                          </div>
                        </div>
                        <div className={styles.divider} />
                      </div>
                    ))}
                  </div>
                </div>
              </Card>
            </div>
            <div className={styles.button_section}>
              <button>Rapor Oluştur</button>
            </div>
          </div>
        </div>
      ) : (
        <div className={styles.form_container}>
          <div className={styles.wallpaper_section}>
            {/* <img src={wallpaper} alt="Interns" /> */}
          </div>
          <div className={styles.content}>
            <img src={Logo} alt="Privia Security" width={250} />

            <div className={styles.form_wrapper}>
              {verificationType === "identity" && (
                <form onSubmit={verifyWithIdentity}>
                  <div className={styles.field}>
                    <input
                      type="text"
                      placeholder="TC Kimlik Numarası"
                      className={styles.input}
                      name="identity_number"
                      value={identityForm.identity_number}
                      onChange={handleChange}
                      required
                    />
                  </div>
                  <div className={styles.field}>
                    <input
                      type="text"
                      placeholder="Doğum tarihi"
                      className={styles.input}
                      name="date_of_birth"
                      value={identityForm.date_of_birth}
                      onChange={handleChange}
                      required
                    />
                  </div>
                  <div className={styles.button_section}>
                    <button disabled={loading} type="submit">
                      {loading ? (
                        <ImSpinner8 className={styles.spinner} size={20} />
                      ) : (
                        "Doğrula"
                      )}
                    </button>
                  </div>
                </form>
              )}
              {verificationType === "uuid" && (
                <form onSubmit={() => verifyWithUuid(uuid)}>
                  <div className={styles.field}>
                    <input
                      type="text"
                      placeholder="Id numarası"
                      className={styles.input}
                      name="uuid"
                      value={uuid.uuid}
                      onChange={(e) => setUuid({ uuid: e.target.value })}
                    />
                  </div>
                  <div className={styles.button_section}>
                    <button disabled={loading} type="submit">
                      Doğrula
                    </button>
                  </div>
                </form>
              )}
            </div>
            <div className={styles.divider} />
            <div className={styles.sub_link}>
              {verificationType === "identity" && (
                <span onClick={() => setVerificationType("uuid")}>
                  Id numarası ile doğrula
                </span>
              )}
              {verificationType === "uuid" && (
                <span onClick={() => setVerificationType("identity")}>
                  TC Kimlik numarası ile doğrula
                </span>
              )}
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Verification;
