import { Card, Col, Row, Spin } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import DemoBarChart from "../../components/Charts/DemoBarChart/DemoBarChart";
import DepartmentChart from "../../components/Charts/DepartmentChart/DepartmentChart";
import ProjectsChart from "../../components/Charts/ProjectsChart/ProjectsChart";
import TopInterns from "../../components/TopInterns/TopInterns";
import TotalValues from "../../components/TotalValues/TotalValues";
import { fetchDashboardData } from "../../store/actions/dashboardActions";
import styles from "./Dashboard.module.scss";
import CustomTopBarProgress from "../../components/CustomTopBarProgress/CustomTopBarProgress";

const Dashboard = () => {
  const dispatch = useDispatch();
  const { data, loading, error } = useSelector((state) => state.dashboard);

  useEffect(() => {
    dispatch(fetchDashboardData());
  }, []);

  if (loading) {
    return (
      <div
        style={{
          height: "100vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spin size="large" />
      </div>
    );
  }

  return (
    <>
      {loading && <CustomTopBarProgress />}
      <div className={styles.container}>
        <div className={styles.left}>
          <TotalValues data={data} loading={loading} error={error} />
          <ProjectsChart data={data} loading={loading} error={error} />
          <div className={styles.flexCharts}>
            <DepartmentChart data={data} loading={loading} error={error} />
            <DemoBarChart data={data} loading={loading} error={error} />
          </div>
        </div>
        <div className={styles.right}>
          <TopInterns data={data} loading={loading} error={error} />
        </div>
      </div>
    </>
  );
};

export default Dashboard;
