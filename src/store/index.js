import { combineReducers } from "redux";

import { internReducer } from "./reducers/internReducer";
import { projectReducer } from "./reducers/projectReducer";
import { universityReducer } from "./reducers/universityReducer";
import { userReducer } from "./reducers/userReducer";
import dashboardReducer from "./reducers/dashboardReducer";

export const rootReducer = combineReducers({
  intern: internReducer,
  university: universityReducer,
  user: userReducer,
  project: projectReducer,
  dashboard: dashboardReducer
});
