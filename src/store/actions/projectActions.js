import { notification } from "antd";
import api from "../../utils/api";
import { showError, showSuccess } from "../../utils/messages";
import serialize from "../../utils/serialize";
import {
  ADD_PROJECT_START,
  ADD_PROJECT_SUCCESS,
  ADD_PROJECT_ERROR,
  EDIT_PROJECT_SUCCESS,
  EDIT_PROJECT_START,
  EDIT_PROJECT_ERROR,
  DELETE_PROJECT_START,
  DELETE_PROJECT_SUCCESS,
  DELETE_PROJECT_ERROR,
  FETCH_PROJECTS_START,
  FETCH_PROJECTS_ERROR,
  FETCH_PROJECTS_SUCCESS,
  GET_PROJECT_BY_INTERN_START,
  GET_PROJECT_BY_INTERN_ERROR,
  GET_PROJECT_BY_INTERN_SUCCESS,
  GET_PROJECT_BY_ID_START,
  GET_PROJECT_BY_ID_SUCCESS,
  GET_PROJECT_BY_ID_ERROR,
} from "./actionTypes";

export const fetchProjects = () => async (dispatch) => {
  dispatch({ type: FETCH_PROJECTS_START });
  try {
    const response = await api().get("/projects");
    dispatch({
      type: FETCH_PROJECTS_SUCCESS,
      payload: response.data.projects,
    });
  } catch {
    dispatch({
      type: FETCH_PROJECTS_ERROR,
      payload: "Intern fetching failed",
    });
  }
};

export const getProjectsByInternId = (id) => async (dispatch) => {
  dispatch({ type: GET_PROJECT_BY_INTERN_START });
  try {
    const { data } = await api().get(
      `/interns/${id}/projects/get_by_intern_id`
    );
    dispatch({ type: GET_PROJECT_BY_INTERN_SUCCESS, payload: data.projects });
  } catch (error) {
    dispatch({ type: GET_PROJECT_BY_INTERN_ERROR });
  }
};

export const getProjectById = (id) => async (dispatch) => {
  dispatch({ type: GET_PROJECT_BY_ID_START });

  try {
    const { data } = await api().get(`/projects/${id}`);
    dispatch({ type: GET_PROJECT_BY_ID_SUCCESS, payload: data.projects });
  } catch (error) {
    dispatch({ type: GET_PROJECT_BY_ID_ERROR });
  }
};

export const addProject = (data) => async (dispatch) => {
  dispatch({ type: ADD_PROJECT_START });

  try {
    const response = await api().post("/projects", data);
    dispatch({
      type: ADD_PROJECT_SUCCESS,
      payload: response.data.projects,
    });
    showSuccess("Proje başarıyla eklendi");
  } catch (error) {
    dispatch({ type: ADD_PROJECT_ERROR });
    (error.response &&
      error.response.data?.message.map((err) => {
        showError(err);
      })) ||
      showError("Proje ekleme başarısız");
  }
};

export const editProject = (id, data) => async (dispatch) => {
  dispatch({ type: EDIT_PROJECT_START });
  try {
    const response = await api().put(`/projects/${id}`, data);
    dispatch({ type: EDIT_PROJECT_SUCCESS, payload: response.data.project });
    showSuccess("Proje başarıyla güncellendi");
  } catch (error) {
    dispatch({ type: EDIT_PROJECT_ERROR });

    (error.response &&
      error.response.data?.message.map((err) => {
        showError(err);
      })) ||
      showError("Proje güncelleme başarısız");
  }
};

export const deleteProject = (id) => async (dispatch) => {
  dispatch({ type: DELETE_PROJECT_START });
  try {
    await api().delete(`/projects/${id}`);
    dispatch({ type: DELETE_PROJECT_SUCCESS, payload: id });
    showSuccess("Proje silindi");
  } catch (error) {
    dispatch({ type: DELETE_PROJECT_ERROR });
    (error.response.data?.message.length &&
      error.response.data?.message.map((err) => {
        showError(err);
      })) ||
      showError("Proje silme başarısız");
  }
};
