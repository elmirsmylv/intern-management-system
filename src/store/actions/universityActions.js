import api from "../../utils/api";
import { showError, showSuccess } from "../../utils/messages";
import {
  ADD_UNI_DEPART_ERROR,
  ADD_UNI_DEPART_START,
  ADD_UNI_DEPART_SUCCESS,
  ADD_UNI_ERROR,
  ADD_UNI_START,
  ADD_UNI_SUCCESS,
  DELETE_UNI_DEPART_ERROR,
  DELETE_UNI_DEPART_START,
  DELETE_UNI_DEPART_SUCCESS,
  DELETE_UNI_ERROR,
  DELETE_UNI_START,
  DELETE_UNI_SUCCESS,
  EDIT_UNI_DEPART_ERROR,
  EDIT_UNI_DEPART_START,
  EDIT_UNI_DEPART_SUCCESS,
  EDIT_UNI_ERROR,
  EDIT_UNI_START,
  EDIT_UNI_SUCCESS,
  FETCH_UNIVERSITIES_ERROR,
  FETCH_UNIVERSITIES_START,
  FETCH_UNIVERSITIES_SUCCESS,
  FETCH_UNI_DEPART_ERROR,
  FETCH_UNI_DEPART_START,
  FETCH_UNI_DEPART_SUCCESS,
} from "./actionTypes";

export const fetchUniversities = () => async (dispatch) => {
  dispatch({ type: FETCH_UNIVERSITIES_START });
  try {
    const { data } = await api().get("/universities");
    dispatch({ type: FETCH_UNIVERSITIES_SUCCESS, payload: data.universities });
  } catch {
    dispatch({ type: FETCH_UNIVERSITIES_ERROR });
  }
};

export const addUniversity = (form) => async (dispatch) => {
  dispatch({ type: ADD_UNI_START });
  try {
    const { data } = await api().post("/universities", form);
    dispatch({ type: ADD_UNI_SUCCESS, payload: data.university });
    showSuccess("Üniversite ekleme başarılı");
  } catch (error) {
    dispatch({ type: ADD_UNI_ERROR });
    error.response.data?.message.map((err) => {
      showError(err);
    }) || showError("Univeriste ekleme başarısız");
  }
};

export const editUniversity = (id, form) => async (dispatch) => {
  dispatch({ type: EDIT_UNI_START });
  try {
    const { data } = await api().put(`/universities/${id}`, form);
    dispatch({ type: EDIT_UNI_SUCCESS, payload: data.university });
    showSuccess("Üniversite güncelleme başarılı");
  } catch (error) {
    dispatch({ type: EDIT_UNI_ERROR });
    error.response.data?.message.map((err) => {
      showError(err);
    }) || showError("Universite güncelleme başarısız");
  }
};

export const deleteUniversity = (id) => async (dispatch) => {
  dispatch({ type: DELETE_UNI_START });
  try {
    await api().delete(`/universities/${id}`);
    dispatch({ type: DELETE_UNI_SUCCESS, payload: id });
    showSuccess("Üniversite başarıyla silindi");
  } catch (error) {
    dispatch({ type: DELETE_UNI_ERROR });
    error.response.data?.message.map((err) => {
      showError(err);
    }) || showError("Universite silme başarısız");
  }
};

// Department actions
export const fetchUniDepartments = () => async (dispatch) => {
  dispatch({ type: FETCH_UNI_DEPART_START });
  try {
    const { data } = await api().get("/university_departments");
    dispatch({
      type: FETCH_UNI_DEPART_SUCCESS,
      payload: data.university_departments,
    });
  } catch (error) {
    dispatch({
      type: FETCH_UNI_DEPART_ERROR,
      payload: "University departments fetching failed", // TODO error messages
    });
  }
};

export const addUniDepartment = (form) => async (dispatch) => {
  dispatch({ type: ADD_UNI_DEPART_START });

  try {
    const { data } = await api().post("/university_departments", form);
    dispatch({
      type: ADD_UNI_DEPART_SUCCESS,
      payload: data.university_department,
    });
    showSuccess("Bölüm başarıyla eklendi");
  } catch (error) {
    dispatch({
      type: ADD_UNI_DEPART_ERROR,
      payload: "Department adding failed",
    });
    (error.response.data.message &&
      error.response.data.message.map((err) => showError(err))) ||
      showError("Bölüm ekleme başarısız");
  }
};

export const editUniDepartment = (id, form) => async (dispatch) => {
  dispatch({ type: EDIT_UNI_DEPART_START });
  try {
    const { data } = await api().put(`/university_departments/${id}`, form);
    dispatch({
      type: EDIT_UNI_DEPART_SUCCESS,
      payload: data.university_department,
    });
    showSuccess("Bölüm başarıyla güncellendi");
  } catch (error) {
    dispatch({
      type: EDIT_UNI_DEPART_ERROR,
      payload: "University department editing failed",
    });

    (error.response.data.message &&
      error.response.data.message.map((err) => showError(err))) ||
      showError("Bölüm güncelleme başarısız");
  }
};

export const deleteUniDepartment = (id) => async (dispatch) => {
  dispatch({ type: DELETE_UNI_DEPART_START });
  try {
    await api().delete(`/university_departments/${id}`);
    dispatch({ type: DELETE_UNI_DEPART_SUCCESS, payload: id });
  } catch (error) {
    dispatch({
      type: DELETE_UNI_DEPART_ERROR,
      payload: "University department deleting failed",
    });
    (error.response.data.message &&
      error.response.data.message.map((err) => showError(err))) ||
      showError("Bölüm silme başarısız");
  }
};
