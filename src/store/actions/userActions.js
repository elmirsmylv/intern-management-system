import axios from "axios";
import SecureLS from "secure-ls";
import api from "../../utils/api";
import { showError } from "../../utils/messages";
import {
  FETCH_USERS_ERROR,
  FETCH_USERS_START,
  FETCH_USERS_SUCCESS,
  GET_USER_ERROR,
  GET_USER_START,
  GET_USER_SUCCESS,
  IS_LOGGED_IN_ERROR,
  IS_LOGGED_IN_START,
  IS_LOGGED_IN_SUCCESS,
  LOGIN_ERROR,
  LOGIN_START,
  LOGIN_SUCCESS,
  LOGOUT,
} from "./actionTypes";

const secureLs = new SecureLS();

export const login = (creds) => async (dispatch) => {
  dispatch({ type: LOGIN_START });
  try {
    const response = await axios.post(
      "https://internverficationn.herokuapp.com/api/v1/auth/sign_in",
      creds
    );
    const {
      "access-token": token,
      client: client,
      uid: uid,
    } = response.headers;

    localStorage.setItem("token", token);
    localStorage.setItem("client", client);
    localStorage.setItem("uid", uid);

    dispatch({ type: LOGIN_SUCCESS, payload: response.data.data });
  } catch (err) {
    dispatch({ type: LOGIN_ERROR });
    (err.response.data.errors &&
      err.response.data.errors.map((err) => {
        showError(err);
      })) ||
      showError("Giriş yapma başarısız");
  }
};

export const isLoggedIn = () => async (dispatch) => {
  dispatch({ type: IS_LOGGED_IN_START });

  // const data = localStorage.getItem("data");
  const token = localStorage.getItem("token");
  const client = localStorage.getItem("client");
  const uid = localStorage.getItem("uid");

  const data = {
    access_token: token,
    client,
    uid,
  };

  try {
    const response = await api().post("/users/check_current_user_token", data);
    dispatch({ type: IS_LOGGED_IN_SUCCESS, payload: response.data.user });
  } catch (err) {
    dispatch({ type: IS_LOGGED_IN_ERROR });
    console.log({ err });
  }
};

export const fetchUsers = () => async (dispatch) => {
  dispatch({ type: FETCH_USERS_START });
  try {
    const response = await api().get("/users");
    dispatch({ type: FETCH_USERS_SUCCESS, payload: response.data.users });
  } catch {
    dispatch({ type: FETCH_USERS_ERROR });
  }
};

export const getSingleUser = (id) => async (dispatch) => {
  dispatch({ type: GET_USER_START });
  try {
    const { data } = await api().get(`/users/${id}`);
    dispatch({ type: GET_USER_SUCCESS, payload: data.user });
  } catch {
    dispatch({ type: GET_USER_ERROR });
  }
};

export const logout = () => (dispatch) => {
  localStorage.removeItem("token");
  localStorage.removeItem("uid");
  localStorage.removeItem("client");
  dispatch({ type: LOGOUT });
};
