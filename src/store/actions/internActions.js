import api from "../../utils/api";
import { showError, showSuccess } from "../../utils/messages";
import serialize from "../../utils/serialize";
import {
  ADD_INTERN_ERROR,
  ADD_INTERN_START,
  ADD_INTERN_SUCCESS,
  DELETE_INTERN_ERROR,
  DELETE_INTERN_START,
  DELETE_INTERN_SUCCESS,
  EDIT_INTERN_ERROR,
  EDIT_INTERN_START,
  EDIT_INTERN_SUCCESS,
  FETCH_INTERNS_ERROR,
  FETCH_INTERNS_START,
  FETCH_INTERNS_SUCCESS,
  GET_INTERN_ERROR,
  GET_INTERN_START,
  GET_INTERN_SUCCESS,
} from "./actionTypes";

export const fetchInterns = () => async (dispatch) => {
  dispatch({ type: FETCH_INTERNS_START });

  try {
    const response = await api().get("/interns");
    dispatch({
      type: FETCH_INTERNS_SUCCESS,
      payload: response.data.interns,
    });
  } catch {
    dispatch({
      type: FETCH_INTERNS_ERROR,
      payload: "Intern fetching failed",
    });
  }
};

export const addIntern = (data) => async (dispatch) => {
  dispatch({ type: ADD_INTERN_START });
  console.log(data);
  try {
    const response = await api().post("/interns", serialize(data));
    dispatch({ type: ADD_INTERN_SUCCESS, payload: response.data.intern });
    showSuccess("Stajer başarıyla eklendi");
  } catch (error) {
    dispatch({ type: ADD_INTERN_ERROR });
    error.response.data?.message.map((err) => {
      showError(err);
    }) || showError("Stajyer ekleme başarısız");
  }
};

export const editIntern = (id, data) => async (dispatch) => {
  dispatch({ type: EDIT_INTERN_START });
  try {
    const response = await api().put(`/interns/${id}`, serialize(data));
    dispatch({ type: EDIT_INTERN_SUCCESS, payload: response.data.intern });
    showSuccess("Stajyer başarıyla güncellendi");
  } catch (error) {
    dispatch({ type: EDIT_INTERN_ERROR });
    (error.response.data.message &&
      error.response.data?.message.map((err) => {
        showError(err);
      })) ||
      showError("Stajyer güncelleme başarısız");
  }
};

export const deleteIntern = (id) => async (dispatch) => {
  dispatch({ type: DELETE_INTERN_START });
  try {
    await api().delete(`/interns/${id}`);
    dispatch({ type: DELETE_INTERN_SUCCESS, payload: id });
    showSuccess("Stajer silindi");
  } catch (error) {
    dispatch({ type: DELETE_INTERN_ERROR });
    error.response?.data.message.map((err) => {
      showError(err);
    }) || showError("Stajyer silme başarısız");
  }
};
