import {FETCH_DASHBOARD_ERROR, FETCH_DASHBOARD_START, FETCH_DASHBOARD_SUCCESS} from "./actionTypes";
import api from "../../utils/api";

export const fetchDashboardData = () => async (dispatch) => {
    dispatch({type:FETCH_DASHBOARD_START});
    try{
        const {data} = await api().get("/dashboard");
        dispatch({type: FETCH_DASHBOARD_SUCCESS, payload: data.dashboard})
    }catch {
        dispatch({type:FETCH_DASHBOARD_ERROR})
    }
}
