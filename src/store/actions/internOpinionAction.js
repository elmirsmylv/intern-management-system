import api from "../../utils/api";
import { showError, showSuccess } from "../../utils/messages";
import serialize from "../../utils/serialize";
import {
    ADD_INTERN_OPINION_START,
    ADD_INTERN_OPINION_SUCCESS,
    ADD_INTERN_OPINION_ERROR,
    EDIT_INTERN_OPINION_SUCCESS,
    EDIT_INTERN_OPINION_START,
    EDIT_INTERN_OPINION_ERROR,
    DELETE_INTERN_OPINION_START,
    DELETE_INTERN_OPINION_SUCCESS,
    DELETE_INTERN_OPINION_ERROR,
    FETCH_INTERN_OPINION_START,
    FETCH_INTERN_OPINION_ERROR,
    FETCH_INTERN_OPINION_SUCCESS
} from "./actionTypes";



export const fetchInternOpnions = () => async (dispatch) => {
    dispatch({ type: FETCH_INTERN_OPINION_START });
    try {
        const response = await api().get("/intern_opinions");
        dispatch({
            type: FETCH_INTERN_OPINION_SUCCESS,
            payload: response.data.projects,
        });
    } catch {
        dispatch({
            type: FETCH_INTERN_OPINION_ERROR,
            payload: "intern opinions fetching failed",
        });
    }
};

export const addOpinion = (data) => async (dispatch) => {
    dispatch({ type: ADD_INTERN_OPINION_START });
    try {
        const response = await api().post("/intern_opinions", serialize(data));
        dispatch({ type: ADD_INTERN_OPINION_SUCCESS, payload: response.data.project });
        showSuccess("proje başarıyla eklendi");
    } catch {
        dispatch({ type: ADD_INTERN_OPINION_ERROR });
    }
};

export const editOpinion = (id, data) => async (dispatch) => {
    dispatch({ type: EDIT_INTERN_OPINION_START });
    try {
        const response = await api().put(`/intern_opinions/${id}`, serialize(data));
        dispatch({ type: EDIT_INTERN_OPINION_SUCCESS, payload: response.data.project });
    } catch {
        dispatch({ type: EDIT_INTERN_OPINION_ERROR });
    }
};

export const deleteInternOpinion = (id) => async (dispatch) => {
    dispatch({ type: DELETE_INTERN_OPINION_START });
    try {
        await api().delete(`/intern_opinions/${id}`);
        dispatch({ type: DELETE_INTERN_OPINION_SUCCESS, payload: id });
        showSuccess("düşünce silindi");
    } catch {
        dispatch({ type: DELETE_INTERN_OPINION_ERROR });
        showError("düşünce silme işlemi başarısız!");
    }
};
