import {
  FETCH_USERS_ERROR,
  FETCH_USERS_START,
  FETCH_USERS_SUCCESS,
  GET_USER_ERROR,
  GET_USER_START,
  GET_USER_SUCCESS,
  IS_LOGGED_IN_ERROR,
  IS_LOGGED_IN_START,
  IS_LOGGED_IN_SUCCESS,
  LOGIN_ERROR,
  LOGIN_START,
  LOGIN_SUCCESS,
  LOGOUT,
} from "../actions/actionTypes";

const initialState = {
  user: null,
  allUsers: [],
  loading: false,
  error: "",
  checkUserPending: false,
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_START:
      return { ...state, loading: true, error: "" };
    case LOGIN_SUCCESS:
      return { ...state, loading: false, user: action.payload };
    case LOGIN_ERROR:
      return { ...state, loading: false, error: action.payload };

    case IS_LOGGED_IN_START:
      return { ...state, checkUserPending: true, error: "" };

    case IS_LOGGED_IN_SUCCESS:
      return { ...state, checkUserPending: false, user: action.payload };
    case IS_LOGGED_IN_ERROR:
      return {
        ...state,
        checkUserPending: false,
        error: "Kullanıcı geçerli değildir. Lütfen tekrar giriş yapınız!",
      };
    case LOGOUT:
      return { ...state, user: null };
    case FETCH_USERS_START:
      return { ...state, loading: true, error: "" };
    case FETCH_USERS_SUCCESS:
      return { ...state, loading: false, allUsers: action.payload };
    case FETCH_USERS_ERROR:
      return { ...state, loading: false, error: "User fetching failed" };
    case GET_USER_START:
      return { ...state, loading: true, error: "" };
    case GET_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        user: action.payload,
      };
    case GET_USER_ERROR:
      return {
        ...state,
        loading: false,
        error: "Kullanıcı getirilemedi!",
      };
    default:
      return state;
  }
};
