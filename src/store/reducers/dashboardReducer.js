import {FETCH_DASHBOARD_ERROR, FETCH_DASHBOARD_START, FETCH_DASHBOARD_SUCCESS} from "../actions/actionTypes";

const initialState = {
    data:{},
    loading:false,
    error:""
}

const dashboardReducer = (state = initialState, action) => {
    switch (action.type){
        case FETCH_DASHBOARD_START:
            return {...state, loading:true, error:''}
        case FETCH_DASHBOARD_SUCCESS:
            return {...state, loading:false, data:action.payload}
        case FETCH_DASHBOARD_ERROR:
            return {...state, loading:false, error: "Dashboard fetching failed"}
        default: return state
    }
}

export default dashboardReducer