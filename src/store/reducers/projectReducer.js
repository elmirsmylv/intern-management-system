import {
  ADD_PROJECT_ERROR,
  ADD_PROJECT_START,
  ADD_PROJECT_SUCCESS,
  DELETE_PROJECT_START,
  DELETE_PROJECT_SUCCESS,
  EDIT_PROJECT_ERROR,
  EDIT_PROJECT_START,
  EDIT_PROJECT_SUCCESS,
  FETCH_PROJECTS_ERROR,
  FETCH_PROJECTS_START,
  FETCH_PROJECTS_SUCCESS,
  GET_PROJECT_BY_ID_ERROR,
  GET_PROJECT_BY_ID_START,
  GET_PROJECT_BY_ID_SUCCESS,
  GET_PROJECT_BY_INTERN_ERROR,
  GET_PROJECT_BY_INTERN_START,
  GET_PROJECT_BY_INTERN_SUCCESS,
} from "../actions/actionTypes";

const initialState = {
  allProjects: [],
  projectsByIntern: [],
  projectById: {},
  loading: false,
  error: "",
};

export const projectReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PROJECTS_START:
      return { ...state, loading: true, error: "" };
    case FETCH_PROJECTS_SUCCESS:
      return { ...state, loading: false, allProjects: action.payload };
    case FETCH_PROJECTS_ERROR:
      return { ...state, loading: false, error: action.payload };
    case GET_PROJECT_BY_INTERN_START:
      return { ...state, loading: true, error: "" };
    case GET_PROJECT_BY_INTERN_SUCCESS:
      return { ...state, loading: false, projectsByIntern: action.payload };
    case GET_PROJECT_BY_INTERN_ERROR:
      return { ...state, loading: false, error: "Projeler getirilemedi" };
    case GET_PROJECT_BY_ID_START:
      return { ...state, loading: true, error: "" };
    case GET_PROJECT_BY_ID_SUCCESS:
      return { ...state, loading: false, projectById: action.payload };
    case GET_PROJECT_BY_ID_ERROR:
      return { ...state, loading: false, error: "Projeler getirilemedi" };
    case ADD_PROJECT_START:
      return { ...state, loading: true, error: "" };
    case ADD_PROJECT_SUCCESS:
      return {
        ...state,
        loading: false,
        allProjects: [action.payload, ...state.allProjects],
      };
    case ADD_PROJECT_ERROR:
      return { ...state, loading: false, error: action.payload };
    case EDIT_PROJECT_START:
      return { ...state, loading: true, error: "" };
    case EDIT_PROJECT_SUCCESS:
      return {
        ...state,
        loading: false,
        allProjects: state.allProjects.map((item) => {
          return item.id === action.payload.id ? action.payload : item;
        }),
      };
    case EDIT_PROJECT_ERROR:
      return {
        ...state,
        loading: false,
        error: "Project güncelleme başarısız!",
      };
    case DELETE_PROJECT_START:
      return { ...state, loading: true, error: "" };
    case DELETE_PROJECT_SUCCESS:
      return {
        ...state,
        loading: false,
        allProjects: state.allProjects.filter(
          (item) => item.id !== action.payload
        ),
      };
    default:
      return state;
  }
};
