import {
  ADD_UNI_DEPART_ERROR,
  ADD_UNI_DEPART_START,
  ADD_UNI_DEPART_SUCCESS,
  ADD_UNI_ERROR,
  ADD_UNI_START,
  ADD_UNI_SUCCESS,
  DELETE_UNI_DEPART_ERROR,
  DELETE_UNI_DEPART_START,
  DELETE_UNI_DEPART_SUCCESS,
  DELETE_UNI_ERROR,
  DELETE_UNI_START,
  DELETE_UNI_SUCCESS,
  EDIT_UNI_DEPART_ERROR,
  EDIT_UNI_DEPART_START,
  EDIT_UNI_DEPART_SUCCESS,
  EDIT_UNI_ERROR,
  EDIT_UNI_START,
  EDIT_UNI_SUCCESS,
  FETCH_UNIVERSITIES_ERROR,
  FETCH_UNIVERSITIES_START,
  FETCH_UNIVERSITIES_SUCCESS,
  FETCH_UNI_DEPART_ERROR,
  FETCH_UNI_DEPART_START,
  FETCH_UNI_DEPART_SUCCESS,
} from "../actions/actionTypes";

const initialState = {
  universities: [],
  departments: [],
  loading: false,
  error: "",
};

export const universityReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_UNIVERSITIES_START:
      return { ...state, loading: true, error: "" };
    case FETCH_UNIVERSITIES_SUCCESS:
      return { ...state, loading: false, universities: action.payload };
    case FETCH_UNIVERSITIES_ERROR:
      return { ...state, loading: false, error: "Üniversiteler getirilemedi" };
    case ADD_UNI_START:
      return { ...state, loading: true, error: "" };
    case ADD_UNI_SUCCESS:
      return {
        ...state,
        loading: false,
        universities: [action.payload, ...state.universities],
      };
    case ADD_UNI_ERROR:
      return {
        ...state,
        loading: false,
        error: "Üniversite ekleme başarısız!",
      };
    case EDIT_UNI_START:
      return { ...state, loading: true, error: "" };
    case EDIT_UNI_SUCCESS:
      return {
        ...state,
        loading: false,
        universities: state.universities.map((uni) =>
          uni.id === action.payload.id ? action.payload : uni
        ),
      };
    case EDIT_UNI_ERROR:
      return {
        ...state,
        loading: false,
        error: "Üniversite güncelleme başarısız",
      };

    case DELETE_UNI_START:
      return { ...state, loading: true, error: "" };
    case DELETE_UNI_SUCCESS:
      return {
        ...state,
        loading: false,
        universities: state.universities.filter(
          (uni) => uni.id !== action.payload
        ),
      };
    case DELETE_UNI_ERROR:
      return { ...state, loading: false, error: "Üniversite silme başarısız!" };

    case FETCH_UNI_DEPART_START:
      return { ...state, loading: true, error: "" };
    case FETCH_UNI_DEPART_SUCCESS:
      return { ...state, loading: false, departments: action.payload };
    case FETCH_UNI_DEPART_ERROR:
      return { ...state, loading: false, error: action.payload };
    case ADD_UNI_DEPART_START:
      return { ...state, loading: true, error: "" };
    case ADD_UNI_DEPART_SUCCESS:
      return {
        ...state,
        loading: false,
        departments: [action.payload, ...state.departments],
      };
    case ADD_UNI_DEPART_ERROR:
      return { ...state, loading: false, error: action.payload };
    case EDIT_UNI_DEPART_START:
      return { ...state, loading: true, error: "" };
    case EDIT_UNI_DEPART_SUCCESS:
      return {
        ...state,
        loading: false,
        departments: state.departments.map((depart) =>
          depart.id === action.payload.id ? action.payload : depart
        ),
      };
    case EDIT_UNI_DEPART_ERROR:
      return { ...state, loading: false, error: action.payload };
    case DELETE_UNI_DEPART_START:
      return { ...state, loading: true, error: "" };
    case DELETE_UNI_DEPART_SUCCESS:
      return {
        ...state,
        loading: false,
        departments: state.departments.filter(
          (depart) => depart.id !== action.payload
        ),
      };
    case DELETE_UNI_DEPART_ERROR:
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};
