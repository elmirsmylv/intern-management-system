export const criteriaReducer = (state, action) => {
  switch (action.type) {
    case "FETCH_START":
      return { ...state, loading: true, error: "", internCriterias: [] };
    case "FETCH_SUCCESS":
      return { ...state, loading: false, internCriterias: action.payload };
    case "FETCH_ERROR":
      return { ...state, loading: false, error: action.payload };
    case "ADD_CRITERIA_START":
      return { ...state, loading: true, error: "" };
    case "ADD_CRITERIA_SUCCESS":
      return {
        ...state,
        loading: false,
        internCriterias: [...state.internCriterias, action.payload],
      };
    case "ADD_CRITERIA_ERROR":
      return { ...state, loading: false, error: action.payload };
    case "UPDATE_CRITERIA_START":
      return { ...state, loading: true, error: "" };
    case "UPDATE_CRITERIA_SUCCESS":
      return {
        ...state,
        loading: false,
        internCriterias: state.internCriterias.map((item) =>
          item.id === action.payload.id ? action.payload : item
        ),
      };
    case "UPDATE_CRITERIA_ERROR":
      return { ...state, loading: false, error: action.payload };
    case "DELETE_CRITERIA_START":
      return { ...state, loading: true, error: "" };
    case "DELETE_CRITERIA_SUCCESS":
      return {
        ...state,
        loading: false,
        internCriterias: state.internCriterias.filter(
          (item) => item.id !== action.payload
        ),
      };
    case "DELETE_CRITERIA_ERROR":
      return { ...state, loading: false, error: action.payload };
    case "GET_CRITERIAS_START":
      return { ...state, loading: true, error: "" };
    case "GET_CRITERIAS_SUCCESS":
      return { ...state, loading: false, criterias: action.payload };
    case "GET_CRITERIAS_ERROR":
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};
