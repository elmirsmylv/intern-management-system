import {
  ADD_INTERN_ERROR,
  ADD_INTERN_START,
  ADD_INTERN_SUCCESS,
  DELETE_INTERN_ERROR,
  DELETE_INTERN_START,
  DELETE_INTERN_SUCCESS,
  EDIT_INTERN_ERROR,
  EDIT_INTERN_START,
  EDIT_INTERN_SUCCESS,
  FETCH_INTERNS_ERROR,
  FETCH_INTERNS_START,
  FETCH_INTERNS_SUCCESS,
  GET_INTERN_ERROR,
  GET_INTERN_START,
  GET_INTERN_SUCCESS,
} from "../actions/actionTypes";

const initialState = {
  interns: [],
  loading: false,
  error: "",
};

export const internReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_INTERNS_START:
      return { ...state, loading: true, error: "" };
    case FETCH_INTERNS_SUCCESS:
      return { ...state, loading: false, interns: action.payload };
    case FETCH_INTERNS_ERROR:
      return { ...state, loading: false, error: action.payload };
    case ADD_INTERN_START:
      return { ...state, loading: true, error: "" };
    case ADD_INTERN_SUCCESS:
      return {
        ...state,
        loading: false,
        interns: [ action.payload, ...state.interns],
      };
    case ADD_INTERN_ERROR:
      return { ...state, loading: false, error: "Stajer ekleme başarısız!" };
    case EDIT_INTERN_START:
      return { ...state, loading: true, error: "" };
    case EDIT_INTERN_SUCCESS:
      return {...state, loading:false, interns: state.interns.map(item => {
        return item.id === action.payload.id ? action.payload : item
        })}
    case EDIT_INTERN_ERROR:
      return { ...state, loading: false, error: "Güncelleme başarısız" };
    case DELETE_INTERN_START:
      return { ...state, loading: true, error: "" };
    case DELETE_INTERN_SUCCESS:
      return {
        ...state,
        loading: false,
        interns: state.interns.filter((intern) => intern.id !== action.payload),
      };
    case DELETE_INTERN_ERROR:
      return {
        ...state,
        loading: false,
        error: "Stajer silme işlemi başarısız!",
      };
    default:
      return state;
  }
};
