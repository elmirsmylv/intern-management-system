import React from "react";
import ReactDOM from "react-dom";
import { applyMiddleware, createStore } from "redux";
import { rootReducer } from "./store";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import { composeWithDevTools } from "redux-devtools-extension";

import App from "./App";
import { isLoggedIn } from "./store/actions/userActions";
import "antd/dist/antd.css";
import "./index.css";
import "@toast-ui/editor/dist/toastui-editor.css";

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

if (localStorage.getItem("token")) {
  store.dispatch(isLoggedIn());
}

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById("root")
);
