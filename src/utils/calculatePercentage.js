export const calculatePercentage = (startedDate, finishedDate) => {
  let start = new Date(startedDate);
  let end = new Date(finishedDate);
  let today = new Date();

  let result = Math.round(((today - start) / (end - start)) * 100);
  return result;
};
