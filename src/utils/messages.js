import { message, notification } from "antd";

export const showSuccess = (successMessage) => {
  notification.open({
    message: successMessage,
    type:'success',
    duration:4
  });
};

export const showError = (errorMessage) => {
  // message.error(errorMessage);
  notification.open({
    message: errorMessage,
    type:'error',
    duration:4
  });
};
