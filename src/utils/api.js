import axios from "axios";
import SecureLS from "secure-ls";

const secureLs = new SecureLS();

export default () => {
  const token = localStorage.getItem("token");
  const client = localStorage.getItem("client");
  const uid = localStorage.getItem("uid");

  return axios.create({
    baseURL: "https://internverficationn.herokuapp.com/api/v1",
    headers: {
      "Access-Token": token,
      "Token-Type": "Bearer",
      Client: client,
      Uid: uid,
    },
  });
};
