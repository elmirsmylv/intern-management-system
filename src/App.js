import React, { useEffect } from "react";
import { Redirect, Route, Switch, useHistory } from "react-router-dom";

import "./App.css";
import NewOrEditIntern from "./pages/NewOrEditIntern/NewOrEditIntern";
import Interns from "./pages/Interns/Interns";
import LayoutPage from "./pages/Layout/LayoutPage";
import Universities from "./pages/Universities/Universities";
import InternDetail from "./pages/InternDetail/InternDetail";
import Dashboard from "./pages/Dashboard/Dashboard";
import Login from "./pages/Login/Login";
import PrivateRoute from "./components/PrivateRoute/PrivateRoute";
import Projects from "./pages/Projects/Projects";
import Logout from "./components/Logout/Logout";
import { useDispatch, useSelector } from "react-redux";
import { isLoggedIn } from "./store/actions/userActions";
import UserDetail from "./pages/UserDetail/UserDetail";
import { showError } from "./utils/messages";
import { Spin } from "antd";
import ProjectDetail from "./pages/ProjectDetail/ProjectDetail";
import DemoMdEditor from "./components/DemoMdEditor";
import Verification from "./pages/Verification/Verification";

function App() {
  const { user, loading2 } = useSelector((state) => state.user);

  // if (loading2) {
  //   return (
  //     <div
  //       style={{
  //         height: "100vh",
  //         display: "flex",
  //         justifyContent: "center",
  //         alignItems: "center",
  //       }}
  //     >
  //       <Spin size="large" />
  //     </div>
  //   );
  // }

  return (
    <>
      <Switch>
        <Route
          exact
          path="/login"
          render={() => {
            if (user) {
              return <Redirect to="/" />;
            } else {
              return <Login />;
            }
          }}
        />
        <Route exact path="/logout">
          <Logout />
        </Route>
        <Route exact path="/intern_verification" component={Verification} />
        <LayoutPage>
          <PrivateRoute exact path="/dashboard" component={Dashboard} />
          <PrivateRoute exact path="/" component={Dashboard} />
          <PrivateRoute
            exact
            path="/dashboard/intern_detail/:id"
            component={InternDetail}
          />
          <PrivateRoute exact path="/interns" component={Interns} />
          <PrivateRoute
            exact
            path="/interns/edit/:id"
            component={NewOrEditIntern}
          />
          <PrivateRoute exact path="/interns/new" component={NewOrEditIntern} />
          <PrivateRoute exact path="/universities" component={Universities} />
          <PrivateRoute
            exact
            path="/interns/intern_detail/:id"
            component={InternDetail}
          />
          <PrivateRoute
            exact
            path="/projects/intern_detail/:id"
            component={InternDetail}
          />
          <PrivateRoute
            exact
            path="/interns/intern_detail/edit/:id"
            component={NewOrEditIntern}
          />
          <PrivateRoute exact path="/projects" component={Projects} />
          <PrivateRoute exact path="/user_detail/:id" component={UserDetail} />
          <PrivateRoute
            exact
            path="/projects/user_detail/:id"
            component={UserDetail}
          />
          <PrivateRoute
            exact
            path="/projects/project_detail/:id"
            component={ProjectDetail}
          />
          <PrivateRoute
            exact
            path="/projects/project_detail/intern_detail/:id"
            component={InternDetail}
          />
          <PrivateRoute
            exact
            path="/user_detail/intern_detail/:id"
            component={InternDetail}
          />
          <Route exact path="/demo-editor" component={DemoMdEditor} />
        </LayoutPage>
      </Switch>
    </>
  );
}

export default App;
