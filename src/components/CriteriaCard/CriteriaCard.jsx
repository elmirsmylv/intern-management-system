import { Button, Card, Form, Input, InputNumber, Modal, Select } from "antd";

import styles from "./CriteriaCard.module.scss";
import star from "../../assets/images/star.svg";
import addButton from "../../assets/images/addCriteriaButton.svg";
import React, { useEffect, useReducer, useState } from "react";
import api from "../../utils/api";
import { useParams } from "react-router-dom";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { fetchUsers } from "../../store/actions/userActions";
import { useDispatch, useSelector } from "react-redux";
import { criteriaReducer } from "../../store/reducers/criteriaReducer";

const Mode = "new" | "edit" | "delete";

const initialState = {
  internCriterias: [],
  criterias: [],
  loading: false,
  error: "",
};

const Criteria = () => {
  const params = useParams();
  const dispatcher = useDispatch();
  const { allUsers: users, user } = useSelector((state) => state.user);
  const [state, dispatch] = useReducer(criteriaReducer, initialState);
  const { internCriterias, criterias, loading, error } = state;

  const emptyForm = {
    criterion_id: undefined,
    score: undefined,
    user_id: user.id,
    intern_id: undefined,
  };

  const [isVisible, setIsVisible] = useState(false);
  const [mode, setMode] = useState(Mode);
  const [form, setForm] = useState(emptyForm);
  const [id, setId] = useState(undefined);

  const handleOk = () => {
    if (mode === "new") addCriteria(form);
    if (mode === "edit") updateCriteria(id, form);
    if (mode === "delete") deleteCriteria(id);
    setIsVisible(false);
    setForm(emptyForm);
  };

  const handleCancel = () => {
    setIsVisible(false);
    setForm(emptyForm);
  };

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  const showModal = (mode) => {
    setMode(mode);
    setIsVisible(true);
  };

  const addCriteria = async (data) => {
    dispatch({ type: "ADD_CRITERIA_START" });
    await api()
      .post("/intern_criteria", data)
      .then((res) => {
        dispatch({
          type: "ADD_CRITERIA_SUCCESS",
          payload: res.data.intern_criteria,
        });
      })
      .catch((err) => {
        dispatch({
          type: "ADD_CRITERIA_ERROR",
          payload: "Stajyer kriteri ekleme başarısız!",
        });
      });
  };

  const updateCriteria = async (id, data) => {
    dispatch({ type: "UPDATE_CRITERIA_START" });

    await api()
      .put(`/intern_criteria/${id}`, data)
      .then((res) => {
        dispatch({
          type: "UPDATE_CRITERIA_SUCCESS",
          payload: res.data.intern_criteria,
        });
      })
      .catch((err) => {
        dispatch({
          type: "UPDATE_CRITERIA_ERROR",
          payload: "Stajyer kriteri başarıyla silindi",
        });
      });
  };

  const deleteCriteria = async (id) => {
    dispatch({ type: "DELETE_CRITERIA_START" });

    await api()
      .delete(`intern_criteria/${id}`)
      .then((res) => {
        dispatch({ type: "DELETE_CRITERIA_SUCCESS", payload: id });
      })
      .catch((err) => {
        dispatch({
          type: "DELETE_CRITERIA_ERROR",
          payload: "Stajyer kriteri silme başarısız",
        });
      });
  };

  const getCriteriaByIntern = async () => {
    dispatch({ type: "FETCH_START" });

    await api()
      .get(`interns/${params.id}/intern_criteria`)
      .then((res) => {
        dispatch({ type: "FETCH_SUCCESS", payload: res.data.intern_criteria });
      })
      .catch((err) => {
        dispatch({
          type: "FETCH_ERROR",
          payload: "Stajyer kriterleri getirilemedi!",
        });
      });
  };

  const getCriterias = async () => {
    dispatch({ type: "GET_CRITERIAS_START" });

    await api()
      .get("/criterions")
      .then(({ data }) => {
        dispatch({ type: "GET_CRITERIAS_SUCCESS", payload: data.criterions });
      })
      .catch((err) => {
        console.log(err);
        dispatch({
          type: "GET_CRITERIAS_ERROR",
          payload: "Kriterler getirilemedi",
        });
      });
  };

  useEffect(() => {
    getCriteriaByIntern();
    getCriterias();
    dispatcher(fetchUsers());
    setForm({ ...form, intern_id: params.id });
  }, []);

  return (
    <>
      <Card className={styles.card} loading={loading}>
        <div className={styles.card_top}>
          <p className={styles.card_title}>Kriterler</p>
          <div onClick={() => showModal("new")} className={styles.add_buttons}>
            <img width={20} src={addButton} className={styles.button} alt="" />
          </div>
        </div>
        <div className={styles.criterias}>
          {internCriterias?.map((criteria) => {
            return (
              <div className={styles.criteria_wrapper} key={criteria.id}>
                <div className={styles.criteria}>
                  <div className={styles.title_description}>
                    <div className={styles.title_section}>
                      <p>
                        Başlık{" "}
                        <span>
                          {criteria.criterion && criteria.criterion.name}
                        </span>
                      </p>
                    </div>
                    <EditOutlined
                      className={styles.edit_icon}
                      onClick={() => {
                        setId(criteria && criteria.id);
                        setForm(criteria);
                        showModal("edit");
                        console.log(id);
                      }}
                    />
                    <DeleteOutlined
                      className={styles.delete_icon}
                      onClick={() => {
                        showModal("delete");
                        setId(criteria && criteria.id);
                      }}
                    />
                  </div>
                  <div className={styles.rating}>
                    <img width={18} src={star} alt="Star Logo" />
                    <p>{criteria.score.toFixed(1)}</p>
                  </div>
                </div>
                <hr />
              </div>
            );
          })}
        </div>
      </Card>

      <Modal
        title={
          mode === "new"
            ? "Kriter ekle"
            : mode === "edit"
            ? "Kriteri güncelle"
            : "Kriteri sil"
        }
        visible={isVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okButtonProps={{ style: { borderRadius: "10px" } }}
        cancelButtonProps={{ style: { borderRadius: "10px" } }}
        okText={mode === "new" ? "Ekle" : mode === "edit" ? "Güncelle" : "Sil"}
        cancelText="Vazgeç"
      >
        {mode !== "delete" ? (
          <Form labelCol={{ span: 5 }} wrapperCol={{ span: 18 }}>
            <Form.Item label="Kriter">
              <Select
                value={form.criterion_id}
                onChange={(value) => setForm({ ...form, criterion_id: value })}
              >
                {criterias &&
                  criterias.map((criteria) => (
                    <Select.Option key={criteria.id} value={criteria.id}>
                      {criteria.name}
                    </Select.Option>
                  ))}
              </Select>
            </Form.Item>
            <Form.Item label="Score">
              <InputNumber
                value={form.score}
                min={0}
                max={10}
                style={{ borderRadius: "10px" }}
                onChange={(value) => setForm({ ...form, score: value })}
              />
            </Form.Item>
            <Form.Item label="Mentörler">
              <Select
                value={form.user_id}
                onChange={(value) => setForm({ ...form, user_id: value })}
              >
                {users?.map((user) => {
                  return (
                    <Select.Option key={user.id} value={user.id}>
                      {user.name} {user.surname}
                    </Select.Option>
                  );
                })}
              </Select>
            </Form.Item>
          </Form>
        ) : (
          <>Kriteri silmeye emin misiniz?</>
        )}
      </Modal>
    </>
  );
};

export default Criteria;
