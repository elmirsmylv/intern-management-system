import React from "react";
import { useSelector } from "react-redux";
import { Redirect, Route } from "react-router-dom";
import Login from "../../pages/Login/Login";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const { user, checkUserPending } = useSelector((state) => state.user);

  if (checkUserPending) {
    return null;
  }

  return (
    <Route
      {...rest}
      render={(props) => {
        return user ? <Component {...props} /> : <Redirect to="/login" />;
      }}
    />
  );
};

export default PrivateRoute;
