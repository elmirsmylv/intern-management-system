import { EditOutlined, UserOutlined } from "@ant-design/icons";
import { Avatar, Card, Tag } from "antd";
import React, { useEffect } from "react";
import { useHistory } from "react-router";
import { Link, useLocation, useParams } from "react-router-dom";
import { BsCloudDownload } from "react-icons/bs";

import style from "../../pages/InternDetail/InternDetail.module.scss";
import { Viewer } from "@toast-ui/react-editor";

// PARAMS
// url => to have a link at title or not
// width
// data={intern}
// loading={loading}
// isNoteVisible

const ProfileCard = ({ data, loading, width, url, isNoteVisible }) => {
  const history = useHistory();

  let color = "blue";
  if (
    data.company_department &&
    data.company_department.name === "Backend Developer"
  )
    color = "blue";
  if (
    data.company_department &&
    data.company_department.name === "Frontend Developer"
  )
    color = "green";
  if (data.company_department && data.company_department.name === "Security")
    color = "orange";
  if (data.company_department && data.company_department.name === "Design")
    color = "pink";

  return (
    <div className={style.profile_card_container}>
      <Card className={style.profile} loading={loading}>
        <div className={style.title_section}>
          <div
            className={
              data.status === "Active"
                ? style.avatar_active
                : data.status === "Deactive"
                ? style.avatar_deactive
                : style.black_list
            }
          >
            <Avatar size={60} icon={<UserOutlined />} />
          </div>
          <div className={style.title_subtitle}>
            {url ? (
              <Link to={`/user_detail/intern_detail/${data.id}`}>
                <h3>
                  {data.name} {data.surname}
                </h3>
              </Link>
            ) : (
              <h3>
                {data.name} {data.surname}
              </h3>
            )}

            <p>Stajyer</p>
            <Tag color={color} className={style.tag}>
              {data.company_department && data.company_department.name}
            </Tag>
          </div>
        </div>
        <div
          style={{ marginLeft: "0.5rem", marginTop: "1rem" }}
          className="content"
        >
          <p className={style.content_item}>
            Başlangıç Tarihi: <span>{data.started_date}</span>
          </p>
          <p className={style.content_item}>
            Bitiş Tarihi: <span>{data.finished_date}</span>
          </p>
          <p className={style.content_item}>
            Rating: <span>{data.score ? data.score.toFixed(1) : "--"}</span>
          </p>
          <p className={style.content_item}>
            CV:{" "}
            <span>
              <button className={style.download_cv}>
                <a href={data.cv} target="_blank">
                  Indir <BsCloudDownload />
                </a>
              </button>
            </span>
          </p>
          {isNoteVisible && (
            <>
              <p className={style.content_item}>Notlar:</p>
              <p className={style.content_item}>
                <Viewer
                  initialValue={
                    data.intern_opinion ? data.intern_opinion.description : "--"
                  }
                />
              </p>
            </>
          )}
        </div>
        <div
          className={style.edit_button}
          onClick={() =>
            history.push({
              pathname: `/interns/intern_detail/edit/${data.id}`,
              state: { mode: "edit" },
            })
          }
        >
          <EditOutlined />
        </div>
      </Card>
    </div>
  );
};

export default ProfileCard;

{
  /* <div className={style.profile}>
      <Card loading={loading} style={{ width: 315 }}>
        <div className={style.title_section}>
          <div
            className={
              data.status
                ? style.avatar_active
                : !data.status
                ? style.avatar_deactive
                : style.avatar_deactive
            }
          >
            <Avatar size={60} icon={<UserOutlined />} />
          </div>
          <div className={style.title_subtitle}>
            <h3>
              {data.name} {data.surname}
            </h3>
            <p>Stajyer</p>
            <Tag color={color} className={style.tag}>
              {data.department}
            </Tag>
          </div>
        </div>
        <div
          style={{ marginLeft: "0.5rem", marginTop: "1rem" }}
          className="content"
        >
          <p className={style.content_item}>
            Başlangıç Tarihi: <span>{data.started_date}</span>
          </p>
          <p className={style.content_item}>
            Bitiş Tarihi: <span>{data.finished_date}</span>
          </p>
          <p className={style.content_item}>
            Rating: <span>{data.score ? data.score.toFixed(1) : "--"}</span>
          </p>
          <p className={style.content_item}>Notlar:</p>
          <p className={style.content_item}>
            {data.intern_opinion ? data.intern_opinion.description : "--"}
          </p>
        </div>
      </Card>
      <div
        className={style.edit_button}
        onClick={() =>
          history.push({
            pathname: `/interns/intern_detail/edit/${data.id}`,
            state: { mode: "edit" },
          })
        }
      >
        <EditOutlined />
      </div>
    </div> */
}
