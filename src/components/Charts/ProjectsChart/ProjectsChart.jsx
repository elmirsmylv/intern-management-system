import { Card } from "antd";
import React, { useEffect, useState } from "react";
import Chart from "react-apexcharts";

const ProjectsChart = ({ data, loading, error }) => {
  const [config, setConfig] = useState({
    series: [
      {
        data: [
          {
            x: "Code",
            y: [
              new Date("2019-03-02").getTime(),
              new Date("2019-03-10").getTime(),
            ],
          },
          {
            x: "Test",
            y: [
              new Date("2019-03-11").getTime(),
              new Date("2019-03-18").getTime(),
            ],
          },
          {
            x: "Validation",
            y: [
              new Date("2019-03-04").getTime(),
              new Date("2019-03-12").getTime(),
            ],
          },
          {
            x: "Deployment",
            y: [
              new Date("2019-03-12").getTime(),
              new Date("2019-03-18").getTime(),
            ],
          },
        ],
      },
    ],
    options: {
      chart: {
        height: 350,
        type: "rangeBar",
      },
      plotOptions: {
        bar: {
          horizontal: true,
        },
      },
      xaxis: {
        type: "datetime",
      },
    },
  });

  const newData = data.upcoming_projects?.map((project) => {
    return {
      x: project.project_name,
      y: [
        new Date(project.started_date).getTime(),
        new Date(project.finished_date).getTime(),
      ],
    };
  });

  useEffect(() => {
    setConfig({
      series: [
        {
          data: newData,
        },
      ],
      options: {
        chart: {
          height: 350,
          type: "rangeBar",
        },
        plotOptions: {
          bar: {
            horizontal: true,
          },
        },
        xaxis: {
          type: "datetime",
        },
      },
    });
  }, [data]);

  return (
    <Card loading={loading} style={{ marginTop: "2rem", borderRadius: "15px" }}>
      <Chart
        options={config.options}
        series={config.series}
        type="rangeBar"
        height={350}
      />
    </Card>
  );
};

export default ProjectsChart;
