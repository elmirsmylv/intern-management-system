import React from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import ReactApexChart from "react-apexcharts";

import { fetchInterns } from "../../../store/actions/internActions";
import { Card } from "antd";

const DepartmentChart = ({ data, loading, error }) => {
  const dispatch = useDispatch();
  const { interns } = useSelector((state) => state.intern);

  let backend = 0;
  let frontend = 0;
  let guvenlik = 0;
  let tasarim = 0;

  interns &&
    interns.forEach((intern) => {
      if (
        intern.company_department &&
        intern.company_department.name === "Backend Developer"
      ) {
        backend += 1;
      } else if (
        intern.company_department &&
        intern.company_department.name === "Frontend Developer"
      ) {
        frontend += 1;
      } else if (
        intern.company_department &&
        intern.company_department.name === "Security"
      ) {
        guvenlik += 1;
      } else if (
        intern.company_department &&
        intern.company_department.name === "Design"
      ) {
        tasarim += 1;
      }
    });

  const series = [backend, frontend, guvenlik, tasarim];
  const options = {
    chart: {
      type: "pie",
      width: "100%",
    },
    labels: ["Backend Developer", "Frontend Developer", "Guvenlik", "Tasarim"],
  };

  useEffect(() => {
    dispatch(fetchInterns());
  }, []);

  return (
    <>
      <Card loading={loading} style={{ borderRadius: "15px" }}>
        <ReactApexChart
          options={options}
          series={series}
          type="pie"
          height={300}
        />
      </Card>
    </>
  );
};

export default DepartmentChart;
