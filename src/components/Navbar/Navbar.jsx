import React, { useState } from "react";
import { Layout, Input, Avatar, Typography } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { BiSearch } from "react-icons/bi";
import { AiOutlineClose } from "react-icons/ai";
import { useSelector } from "react-redux";
import { useEffect } from "react";

import styles from "./Navbar.module.scss";
import { useHistory } from "react-router";

const { Header } = Layout;
const { Paragraph } = Typography;

const Navbar = ({ collapsed, toggle }) => {
  const { user, loading, error } = useSelector((state) => state.user);
  const history = useHistory();

  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {}, []);

  return (
    <Header
      className="site-layout-background"
      style={{
        padding: 0,
        backgroundColor: "#fff",
        alignItems: "center",
        display: "flex",
      }}
    >
      {collapsed ? (
        <MenuUnfoldOutlined
          style={{ marginLeft: "1rem", fontSize: "1.25rem" }}
          onClick={() => toggle()}
        />
      ) : (
        <MenuFoldOutlined
          style={{ marginLeft: "1rem", fontSize: "1.25rem" }}
          onClick={() => toggle()}
        />
      )}
      <div
        className="nav-content"
        style={{
          display: "flex",
          justifyContent: "flex-end",
          width: "100%",
        }}
      >
        <div
          className={styles.userInfo}
          style={{
            display: "flex",
            alignItems: "center",
            marginRight: "1.85rem",
            cursor: "pointer",
          }}
          onClick={() => {
            history.push(`/user_detail/${user.id}`);
          }}
        >
          <Avatar size={40} icon={<UserOutlined />} className={styles.avatar} />
          <Paragraph
            style={{ marginLeft: "10px", marginTop: "10px" }}
            level={5}
          >
            {user?.name} {user?.surname}
          </Paragraph>
        </div>
      </div>
    </Header>
  );
};

export default Navbar;
