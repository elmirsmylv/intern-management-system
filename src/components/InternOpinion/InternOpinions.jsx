import React, { useEffect, useState } from 'react'
import { useParams } from "react-router";
import { Table, Tag, Space, Button, Form, Input, Select, Modal } from "antd";
import { useDispatch } from "react-redux";
import { deleteInternOpinion, fetchInternOpnions } from '../../store/actions/internOpinionAction';
import api from "../../utils/api";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
export default function InternOpinions() {

  let { id } = useParams();

  const dispatch = useDispatch();

  const [internOpinions, setInternOpinions] = useState([])

  useEffect(() => {
    getInternOpinionById()
  }, [])

  const getInternOpinionById = async () => {
    const { data } = await api().get(`/interns/${id}/intern_opinions`);
    setInternOpinions(data.intern_opinions);
  };

  const deletOpinion = (internOpinionId) => {
    dispatch(deleteInternOpinion(internOpinionId))
    getInternOpinionById()

  }
  const columns = [
    {
      title: "Başlık",
      dataIndex: "title",
      key: "title",
    },
    {
      title: "Açıklama",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "Score",
      dataIndex: "score",
      key: "score"
    },
    {
      title: "İşlem",
      key: "action",
      render: (text, internOpinions) => (
        <Space>
          <EditOutlined
            style={{ color: "#40A9FF" }}

          />
          <DeleteOutlined
            onClick={() => deletOpinion(internOpinions.id)}
            style={{ color: "#c70d00" }}
          />
        </Space>
      ),
    },
  ];
  return (
    <div>
      <Table
        columns={columns}
        dataSource={internOpinions}
        rowKey="id"
      />
    </div>
  )
}
