import { Input } from "antd";
import React, { useState } from "react";

// You have to keep state in other component for data. There is example usage in Universities.jsx at line 125.
// data => real data
// setData => state of real data
const SearchBar = ({ data, setData, placeholder }) => {
  const [value, setValue] = useState();
  return (
    <div style={{ display: "flex", justifyContent: "space-between" }}>
      <Input
        placeholder={placeholder}
        value={value}
        style={{ borderRadius: "10px", marginLeft: "0.7rem" }}
        onChange={(e) => {
          const currValue = e.target.value.toLowerCase();
          setValue(currValue);
          const filteredData = data.filter((entry) => {
            return entry.name
              ? entry.name.toLowerCase().includes(currValue) // Berat will change project_name with "name" at projects table on DB.
              : entry.project_name.toLowerCase().includes(currValue);
          });
          setData(filteredData);
        }}
      />
    </div>
  );
};

export default SearchBar;
