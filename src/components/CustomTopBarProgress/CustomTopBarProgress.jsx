import React from "react";
import TopBarProgress from "react-topbar-progress-indicator";

export default function CustomTopBarProgress() {
  TopBarProgress.config({
    barColors: {
      0: "#1b4f70",
      "1.0": "#1b4f70",
    },
  });

  return <TopBarProgress />;
}
