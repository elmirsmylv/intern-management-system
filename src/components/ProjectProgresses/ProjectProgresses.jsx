import React, { useEffect, useState } from "react";
import { Card, Modal, Progress, Form, Input, DatePicker, Select } from "antd";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";

import styles from "../../pages/InternDetail/InternDetail.module.scss";
import {
  addProject,
  getProjectsByInternId,
} from "../../store/actions/projectActions";
import addButton from "../../assets/images/addCriteriaButton.svg";
import { fetchInterns } from "../../store/actions/internActions";
import { fetchUsers } from "../../store/actions/userActions";
import { calculatePercentage } from "../../utils/calculatePercentage";

const emptyForm = {
  project_name: "",
  started_date: "",
  finished_date: "",
  description: "",
  user_ids: [],
  intern_ids: [],
};

const ProjectProgresses = ({ internId }) => {
  const params = useParams();
  const dispatch = useDispatch();
  const {
    projectsByIntern: data,
    allProjects,
    loading,
    error,
  } = useSelector((state) => state.project);
  const { interns, loading: internLoading } = useSelector(
    (state) => state.intern
  );
  const { allUsers: users } = useSelector((state) => state.user);

  const [isVisible, setIsVisible] = useState(false);
  const [form, setForm] = useState(emptyForm);

  const handleOk = () => {
    dispatch(addProject(form));
    setIsVisible(false);
    setForm(emptyForm);
  };

  const handleCancel = () => {
    setIsVisible(false);
    setForm(emptyForm);
  };

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  const showModal = () => {
    setIsVisible(true);
  };

  // const calculatePercentage = (startedDate, finishedDate) => {
  //   let start = new Date(startedDate);
  //   let end = new Date(finishedDate);
  //   let today = new Date();

  //   let result = Math.round(((today - start) / (end - start)) * 100);
  //   return result;
  // };

  useEffect(() => {
    dispatch(getProjectsByInternId(internId));
    dispatch(fetchInterns());
    dispatch(fetchUsers());
  }, []);

  useEffect(() => {
    dispatch(getProjectsByInternId(internId));
  }, [allProjects]);

  return (
    <>
      <Card loading={loading} className={styles.progresses_card}>
        <div className={styles.card_top}>
          <p className={styles.card_title}>Eklendiği Projeler</p>
          {/* <div onClick={showModal}>
            <img
              width={20}
              src={addButton}
              className={styles.button}
              alt="Add Button"
            />
          </div> */}
        </div>
        {data?.map((project) => {
          return (
            <div key={project.id} className={styles.project}>
              <div className={styles.project_titles}>
                <div className={styles.project_name}>
                  <p>Proje Adı</p>
                  <span> {project.project_name}</span>
                </div>
                <div className={styles.project_user}>
                  <p>Proje Mentörü </p>
                  {project.users.map((user) => (
                    <span key={user.id}>
                      {user.name} {user.surname}{" "}
                    </span>
                  ))}
                </div>
                <div className={styles.project_description}>
                  <p>Proje Aciklamasi</p>
                  <span className={styles.description_content}>
                    {project.description}
                  </span>
                </div>
              </div>
              <Progress
                strokeColor={{
                  from: "#108ee9",
                  to: "#87d068",
                }}
                percent={calculatePercentage(
                  project.started_date,
                  project.finished_date
                )}
                status={
                  calculatePercentage(
                    project.started_date,
                    project.finished_date
                  ) > 99
                    ? "success"
                    : "active"
                }
              />
              <div className={styles.dates}>
                <p>Başlangıç Tarihi: {project.started_date}</p>
                <p>Bitiş Tarihi: {project.finished_date}</p>
              </div>
            </div>
          );
        })}
      </Card>

      <Modal
        title="Proje Oluştur"
        okButtonProps={{ style: { borderRadius: "10px" } }}
        cancelButtonProps={{ style: { borderRadius: "10px" } }}
        onOk={handleOk}
        onCancel={handleCancel}
        visible={isVisible}
        okText="Oluştur"
        cancelText="Vazgeç"
        width={600}
      >
        <div className={styles.new_project_section}>
          <div className={styles.new_project}>
            <h3 className={styles.new_project_name}>Stajyer Dogrulama</h3>
            <button className={styles.add_button}>Ekle</button>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default ProjectProgresses;

{
  /* <Form labelCol={{ span: 5 }} wrapperCol={{ span: 17 }}>
          <Form.Item label="Proje Adı">
            <Input
              style={{ borderRadius: "10px" }}
              value={form.project_name}
              name="project_name"
              onChange={handleChange}
            />
          </Form.Item>
          <Form.Item label="Süre" style={{ marginBottom: "0" }}>
            <Form.Item style={{ display: "inline-block" }}>
              <DatePicker
                className={styles.date_picker}
                placeholder="Başlangıç Tarihi"
                value={form.started_date && moment(form.started_date)}
                onChange={(date, dateString) =>
                  setForm({ ...form, started_date: dateString })
                }
              />
            </Form.Item>
            <Form.Item style={{ display: "inline-block" }}>
              <DatePicker
                className={styles.date_picker}
                placeholder="Bitiş Tarih"
                value={form.finished_date && moment(form.finished_date)}
                onChange={(date, dateString) =>
                  setForm({ ...form, finished_date: dateString })
                }
              />
            </Form.Item>
          </Form.Item>
          <Form.Item label="Stajyerler">
            <Select
              mode="multiple"
              value={form.intern_ids}
              onChange={(values) => setForm({ ...form, intern_ids: values })}
            >
              {interns.map((intern) => {
                if (intern.status) {
                  return (
                    <Select.Option key={intern.id} value={intern.id}>
                      {intern.name} {intern.surname}
                    </Select.Option>
                  );
                }
              })}
            </Select>
          </Form.Item>
          <Form.Item label="Mentörler">
            <Select
              value={form.user_ids}
              mode="multiple"
              onChange={(values) => setForm({ ...form, user_ids: values })}
            >
              {users.map((user) => {
                return (
                  <Select.Option key={user.id} value={user.id}>
                    {user.name} {user.surname}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item label="Proje Açıklaması">
            <Input.TextArea
              value={form.description}
              onChange={handleChange}
              name="description"
              style={{ borderRadius: "10px" }}
            />
          </Form.Item>
        </Form> */
}
