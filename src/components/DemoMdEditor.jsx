import React, { useEffect, useRef, useState } from "react";
import { Editor, Viewer } from "@toast-ui/react-editor";

const DemoMdEditor = () => {
  const [text, setText] = useState("");

  const editorRef = useRef(null);

  const handleChange = () => {
    setText(editorRef.current.getInstance().getMarkdown());
  };

  useEffect(() => {
    console.log(text);
  }, [text]);

  useEffect(() => {
    // editorRef.current.getInstance().changePreviewStyle("vertical");
    console.log(Editor);
  }, []);

  // this component for just make examples before change real component

  return (
    <div style={{ width: "700px" }}>
      <Editor
        initialValue="hello react editor world!"
        previewStyle="tab"
        height="300px"
        ref={editorRef}
        onChange={handleChange}
      />
      <button onClick={handleChange}>Save</button>
      <br />
      <textarea
        style={{ width: "400px" }}
        readOnly="readOnly"
        value={text}
      ></textarea>

      <Viewer initialValue="## hello react editor world!\n\n**tgtsfdggrdegdrfgr**" />
    </div>
  );
};

export default DemoMdEditor;
