import { Card } from "antd";
import React, { useEffect } from "react";
import { HiOutlineUserGroup, HiOutlineStatusOnline } from "react-icons/hi";
import { AiOutlineFundProjectionScreen } from "react-icons/ai";

import styles from "./TotalValues.module.scss";
import users from "../../assets/images/users.svg";
import active from "../../assets/images/active.svg";
import project from "../../assets/images/project.svg";

const TotalValues = ({ data, loading, error }) => {
  return (
    <div className={styles.wrapper}>
      <Card loading={loading} className={styles.card}>
        <div className={styles.content}>
          <div className={styles.infos}>
            <div className={styles.title}>
              <div className={styles.circle}></div>
              <h3>Toplam Stajyer</h3>
            </div>
            <p className={styles.value}>{data.total_interns}</p>
          </div>
          <div className={styles.icon}>
            {/* <HiOutlineUserGroup size={60} color="rgb(151, 151, 151)" /> */}
            <img src={users} alt="Group Users Logo" />
          </div>
        </div>
      </Card>
      <Card loading={loading} className={styles.card}>
        <div className={styles.content}>
          <div className={styles.infos}>
            <div className={styles.title}>
              <div className={styles.circle}></div>
              <h3>Aktif Stajyer</h3>
            </div>
            <p className={styles.value}>{data.active_interns}</p>
          </div>
          <div className={styles.icon}>
            {/* <HiOutlineStatusOnline size={60} color="rgb(151, 151, 151)" /> */}
            <img src={active} alt="" />
          </div>
        </div>
      </Card>
      <Card loading={loading} className={styles.card}>
        <div className={styles.content}>
          <div className={styles.infos}>
            <div className={styles.title}>
              <div className={styles.circle}></div>
              <h3>Toplam Proje</h3>
            </div>
            <p className={styles.value}>{data.total_project}</p>
          </div>
          <div className={styles.icon}>
            {/* <AiOutlineFundProjectionScreen
              size={60}
              color="rgb(151, 151, 151)"
            /> */}
            <img src={project} alt="Project Logo" />
          </div>
        </div>
      </Card>
    </div>
  );
};

export default TotalValues;
