import { Card } from "antd";
import React, { useEffect } from "react";
import { AiOutlineUser } from "react-icons/ai";
import { useLocation } from "react-router";
import { Link } from "react-router-dom";

import styles from "./TopInterns.module.scss";
import star from "../../assets/images/star.svg";

const TopInterns = ({ data, loading, error }) => {
  return (
    <Card loading={loading} className={styles.card}>
      <div className={styles.title_section}>
        <h3>Top Interns</h3>
        <hr />
      </div>
      <div className={styles.interns}>
        {data.the_best_interns?.map((intern) => {
          return (
            <div key={intern.id} className={styles.intern}>
              <div className={styles.name}>
                <AiOutlineUser size="20" />
                <Link
                  className={styles.link}
                  to={`/dashboard/intern_detail/${intern.id}`}
                >
                  <p>{`${intern.name} ${intern.surname}`}</p>
                </Link>
              </div>
              <div className={styles.rating}>
                <img src={star} alt="Star Logo" />
                <p>{intern.score && intern.score.toFixed(1)}</p>
              </div>
            </div>
          );
        })}
      </div>
    </Card>
  );
};

export default TopInterns;
